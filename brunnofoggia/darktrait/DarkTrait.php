<?php

/**
 * A set of needful utilities that make traits a real thing
 * 
 * 1. properties with default values on traits
 * By default PHP does not allow you to have properties extensible on traits, 
 * not without having to resolve conflicts. Using DarkTrait you will be able to 
 * set a list of properties with their default values and classes that use yours
 * will be able to set their properties with custom values
 *
 * 2. global atributte by keyname
 * This feature allows to have a value shared between entities as a static propertie
 * Unfortunately traits cannot have static properties, so when you're two degrees 
 * under traits and need a global value, a static propertie when classes are the 
 * subject, on the first level, you're not able to have it natively
 *
 * @category    Utilities
 * @author      Bruno Foggia
 * @link        https://bitbucket.org/brunnofoggia/darktrait
 */
trait DarkTrait {

    /**
     * Trick used to define default values for properties undefined under the object
     * @param string name
     * @return mixed
     */
    protected function getAttr($name, $forceDefault = false) {
        $formattedName = $this->formatAttrName($name);
        $propertyMethodName = $this->getAttrMethod($formattedName, $name);
        if ($forceDefault === false) {
            if (method_exists($this, $propertyMethodName)) {
                return $this->{$propertyMethodName}();
            } elseif (property_exists($this, $formattedName) && $this->{$formattedName} !== null) {
                return $this->{$formattedName};
            }
        }
        return @$this->getAttrProperty()[$name];
    }

    /**
     * Set a value to an attribute
     * @param string name
     * @param mixed value
     * @return mixed
     */
    protected function setAttr($name, $value) {
        $formattedName = $this->formatAttrName($name);
        $propertyMethodName = $this->setAttrMethod($formattedName, $name);

        if (method_exists($this, $propertyMethodName)) {
            return $this->{$propertyMethodName}($value);
        } else {
            return $this->{$formattedName} = $value;
        }
    }

    /**
     * Explicit a attribute into a property on object
     * @param string name
     * @param mixed value
     * @return mixed
     */
    protected function explicitAttr($name) {
        return $this->setAttr($name, $this->getAttr($name));
    }

    /**
     * Allow property name to be customized
     * @param string $name
     * @return string
     */
    protected function getAttrProperty() {
        return isset($this->attrDefaults) ? $this->attrDefaults : [];
    }

    /**
     * Allow property getter to be provided
     * @param string $name
     * @return string
     */
    protected function getAttrMethod($formattedName, $name) {
        return 'getAttr_' . $formattedName;
    }
    /**
     * Allow property setter to be provided
     * @param string $name
     * @return string
     */
    protected function setAttrMethod($formattedName, $name) {
        return 'setAttr_' . $formattedName;
    }

    /**
     * Allow property name to be customized
     * @param string $name
     * @return string
     */
    protected function formatAttrName($name) {
        return $name;
    }

    /**
     * Import attribute list
     * @param array $attrList
     */
    protected function setAttrList($attrList) {
        foreach ($attrList as $key => $val) {
            if (!is_array($val)) {
                $this->{$key} = $val;
                continue;
            }
            foreach ($val as $itemKey => $itemVal) {
                if (!is_int($itemKey)) {
                    $this->{$key . '.' . $itemKey} = $itemVal;
                } else {
                    (empty($this->{$key}) || !is_array($this->{$key})) && ($this->{$key} = []);
                    $this->{$key}[$itemKey] = $itemVal;
                }
            }
        }
    }

    /**
     * [internal] Set a unconflicteable name
     * @return string
     */
    protected function _getGlobalName() {
        return '_DARKTRAIT'.$_SERVER['REQUEST_TIME'];
}
    
    /**
     * [internal] Set a global var to hold static information of Traits
     * The stack of values will be named based on attr "DarkTraitGlobal" value
     */
    protected function _initGlobal() {
        $gName = $this->_getGlobalName();
        
        global ${$gName};
        empty(${$gName}) && (${$gName} = []);
        empty(${$gName}[$this->getAttr('DarkTraitGlobal')]) && (${$gName}[$this->getAttr('DarkTraitGlobal')] = []);
    }
    
    /**
     * Getter of values into global var to a trait
     * @param string $name
     * @return mixed
     */
    protected function getGlobal($name) {
        $gName = $this->_getGlobalName();
        
        global ${$gName};
        $this->_initGlobal();
        
        return @${$gName}[$this->getAttr('DarkTraitGlobal')][$name];
    }
    
    /**
     * Setter of values into global var to a trait
     * @param string $name
     * @param mixed $value
     * @return mixed
     */
    protected function setGlobal($name, $value) {
        $gName = $this->_getGlobalName();
        
        global ${$gName};
        $this->_initGlobal();
        
        return ${$gName}[$this->getAttr('DarkTraitGlobal')][$name] = $value;
    }

}
