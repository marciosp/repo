# DarkTrait

[![Minimum PHP Version](http://img.shields.io/badge/php-%3E%3D%205.4-8892BF.svg)](https://php.net/)
[![License](https://img.shields.io/badge/license-MIT-4C9182.svg)](https://opensource.org/licenses/MIT)

A set of needful utilities that make traits a real thing

1. properties with default values on traits
By default PHP does not allow you to have properties extensible on traits, 
not without having to resolve conflicts. Using DarkTrait you will be able to 
set a list of properties with their default values and classes that use yours
will be able to set their properties with custom values

2. global atributte by keyname
This feature allows to have a value shared between entities as a static propertie
Unfortunately traits cannot have static properties, so when you're two degrees 
under traits and need a global value, a static propertie when classes are the 
subject, on the first level, you're not able to have it natively.

## default properties usage

1. Add in your class the following code

        use \DarkTrait;

2. Set your attribute list property as shown

        protected $attrDefaults = [
            'test_property' => '1',
        ];
3. On the classes that use your trait, set properties when you need to customize them

        protected $test_property = '2';

4. Capture property values using this

        $this->getAttr('test_property');

4. Set property value using this

        $this->setAttr('test_property', 'some value');

## static properties usage

1. Set your default attrDefaults[DarkTraitGlobal] at the first level trait with a keyvalue (it's name would do)

        protected $attrDefaults = [
            'DarkTraitGlobal' => 'MyTraitName',
        ];

2. use the getGlobal and setGlobal to do the magic

        $this->getGlobal('some_key'); // in Classes world would be like MyClass::$some_key
        $this->setGlobal('some_key', true); // will set true as some_key value

## advanced usage

- You can customize property names to have their on prefixes or suffixes by setting (this will just have effect to the real property names, not on the keys present into 'attrDefaults')

        protected function formatAttrName($name) {
            return 'my_'.$name;
        }

- A getter/setter can be created to every property. For the example bellow a getter for the property 'layout' was created

        protected function getAttr_layout() {
            ...
        }
        protected function setAttr_layout($value) {
            ...
        }

- Want to do some changes inside of an array list attribute. Perhaps will be useful to set the propertie into your instance

        $this->explicitAttr($name);

- Want to import a list of default attribute values from a file maybe? Use this

        $this->setAttrList($array);

- If you already have declared 'attrDefaults' property to another need, you can rename this one to any other name

        protected function getAttrProperty() {
            return isset($this->otherAttrDefaults) ? $this->otherAttrDefaults : [];
        }

- Resolving conflict when using two traits that uses DarkTrait by combining their list of default attributes

        // 1st you need to define which one of the two traits methods will be the one assuming the front, just to solve the conflict
        \TraitOne::formatAttrName insteadof \TraitTwo;
        \TraitOne::getAttr insteadof \TraitTwo;
        \TraitOne::getAttrMethod insteadof \TraitTwo;
        \TraitOne::setAttrList insteadof \TraitTwo;
        
        // 2nd you will rename everything that conflicted between them 
        \TraitOne::formatAttrName as TO_formatAttrName;
        \TraitOne::getAttr as TO_getAttr;
        \TraitOne::getAttrMethod as TO_getAttr;
        \TraitOne::getAttrProperty as TO_getAttrProperty;
        \TraitTwo::formatAttrName as TT_formatAttrName;
        \TraitTwo::getAttr as TT_getAttr;
        \TraitTwo::getAttrMethod as TT_getAttr;
        \TraitTwo::getAttrProperty as TT_getAttrProperty;
        
        // 3rd specify getAttrProperty to combine their attributes and yours (if you have the attributes default list too)
        public function getAttrProperty() {
            // use the code bellow if you do not have a list of default attributes
            $data = array_merge_recursive($this->TO_getAttrProperty(), $this->TT_getAttrProperty());
            // or this one if you do have a list of default attributes
            $data = array_merge_recursive($this->TO_getAttrProperty(), $this->TT_getAttrProperty(), $this->attrDefaults);
            return $data;
    }
