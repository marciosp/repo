<?php

namespace HiMax;

!defined(__NAMESPACE__ . '\TABLE_NAME_USER')
        && define(__NAMESPACE__ . '\TABLE_NAME_USER', constant(__NAMESPACE__ . '\TABLES_PREFFIX') . 'user');

/**
 * HiMax
 *
 * A toolkit implement to authenticate and authorize users actions
 *
 * @package     HiMax
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/himax
 */
trait DBAuthControl {
    use \HiMax\AuthControl;
    
    /**
     * Recover list of instance models
     */
    public function getModels() {
        return $this->model;
    }
    
    /**
     * Check password
     * @access protected
     * @param string $pass
     * @return string
     */
    protected function checkPassword($pass) {
        if (!empty($this->getData('user'))) {
            return $this->model['user']->checkPassword($this->getData('user'), $pass) ? true : \HiMax\Core::ERROR_INVALIDPASSWORD;
        }
        return \HiMax\Core::ERROR_USERNOTFOUND;
    }

    /**
     * Get user by login
     * @access protected
     * @param string $user
     * @return array
     */
    protected function getUserByLogin($user) {
        $user = $this->model['user']->login_getByLogin($user);
        return $user;
    }

}
