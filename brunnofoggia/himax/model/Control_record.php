<?php

namespace HiMax\model;

class Control_record {
    use \doctrine\Dashes\Model;
    protected $modelAttrDefaults = [
        'table' => \HiMax\TABLE_NAME_CONTROL_RECORD,
//        'recursive' => \doctrine\Dashes\HASMANY,
        'foreignKeys' => [
            'share' => [
                'type' => \doctrine\Dashes\HASMANY,
                'key' => 'control_record_id',
                'model' => '\HiMax\model\Control_record_share'
            ],
        ]
    ];
    
    /**
     * Assure that there is a record with information about the record that's being created/edited
     * @param array $data
     * @return array $row
     */
    public function touch($data) {
        $row = $this->getBy($data);
        if(empty($row)) {
            if(\HiMax\Core::getMe()->isLogged()===true) {
                $data['owner'] = (string) @\HiMax\Core::getMe()->getData('user')['user'];
                $data['group'] = \HiMax\Core::getMe()->getMainProfile();
            }
            $rowId = $this->create($data);
            $row = $this->get($rowId);
        } else if(empty($row['owner']) || empty($row['group'])) {
            if(\HiMax\Core::getMe()->isLogged()===true) {
                empty($row['owner']) && ($row['owner'] = (string) @\HiMax\Core::getMe()->getData('user')['user']);
                empty($row['group']) && ($row['group'] = \HiMax\Core::getMe()->getMainProfile());
            }
        
            $this->update($row[$this->getAttr('primaryKey')], $row);
        }
        
        return $row;
    }
    
    public function implodeDefaultList($defaultLists) {
        $result = $defaultLists;
        if(!empty($defaultLists) && is_array($defaultLists)) {
            $result = [];
            foreach($defaultLists as $group => $list) {
                !is_array($list) && ($list = [$list]);
                foreach($list as $value) {
                    $item = [(string) $group, $value];
                    $result[] = implode('_', $item);
                }
            }
            $result = implode(',', $result);
        }
        
        return $result;        
    }
    
    public function explodeDefaultList($value) {
        $result = $value;
        if(!empty($result)) {
            $list = explode(',', $result);
            $result = [];
            foreach($list as $item) {
                $item = explode('_', $item, 2);
                empty($result[$item[0]]) && ($result[$item[0]] = []);
                $result[$item[0]][] = $item[1];
            }
        }
        
        return $result;
    }
    
    public function beforeSave($event, &$data) {
        if(!empty($data['_default'])) {
            $_default = $data['_default'];
            is_array($data['_default']) && ($_default = $this->implodeDefaultList($data['_default']));
            if(($data['default'] !== $_default)) {
                if(!empty($data[$this->getAttr('primaryKey')])) {
                    $data = $this->getRelated($data, \doctrine\Dashes\HASMANY);
                    if(!empty($data['share']))
                    foreach($data['share'] as $x => $y)
                        $data['share'][$x]['delete'] = true;
                }

                if(!empty($data['_default_records'])) {
                    foreach($data['_default_records'] as $x => $y)
                        $data['share'][] = $y;
                    unset($data['_default_records']);
                }
                $data['default'] = $_default;
            }
        } else {
            $data['default'] = '';
        }
    }
    
    public function prepareRelatedData_share($foreignData, $data, $foreignConfig) {
        $keyList = [];
        
        /* erase duplicates */
        if(!empty($foreignData))
        foreach($foreignData as $x => $y) {
            if(!empty($y['delete'])) continue;
            
            $key = [$y['share']];
            if(!empty($y['share_id'])) {
                $key[] = $y['share_id'];
            }
            $key = implode('-', $key);
            
            if(!array_key_exists($key, $keyList)) {
                $keyList[$key] = $x;
            } else {
                $z = $foreignData[$keyList[$key]];
                
                if((int) $y['access'] < (int) $z['access']) {
                    unset($foreignData[$x]);
                } else {
                    unset($foreignData[$keyList[$key]]);
                    $keyList[$key] = $x;
                }
            }
        }
        
        return $foreignData;
    }
}
