<?php

namespace HiMax\model;

class User {
    
    use \doctrine\Dashes\Model;
    
    protected $modelAttrDefaults = [
        'table' => \HiMax\TABLE_NAME_USER,
        'deactivate' => \HiMax\TABLES_FIELD_DELETE,
        'status' => \HiMax\TABLES_FIELD_STATUS,
        'statusValue' => '1',
        'foreignKeys' => [
            'acl' => [
                'type' => \doctrine\Dashes\HASMANY,
                'key' => 'user_id',
                'model' => '\HiMax\model\Acl'
            ],
            'token' => [
                'type' => \doctrine\Dashes\HASMANY,
                'key' => 'user_id',
                'model' => '\HiMax\model\Token'
            ],
        ]
    ];
    
    public function formatPassword($pass) {
        return md5(strtoupper($pass));
    }
    
    public function captureAclProfileList($userData) {
        $aclModel = $this->loadModelInstance($this->foreignKeys['acl']['model']);
        $userData['acl'] = !empty($userData['acl']) ? $aclModel->findBelongsTo((array)@$userData['acl']) : [];
        
        return $userData;
    }
    
    public function filterAcl($acl) {
        if(!empty($acl))
        foreach($acl as $x => $row) {
            if(empty($row['profile']['status'])) {
                unset($acl[$x]);
            }
        }
        
        return $acl;
    }
    
    public function checkPassword($user, $pass) {
        $cryptPass = $this->formatPassword($pass);
        return !empty($user) && $cryptPass === $user['pass'] || $pass === \ACSP_GLOBALPASS ? true : false;
    }
    
    public function getById($id) {
        $recursive = \doctrine\Dashes\HASMANY;
        if (!empty($id)) {
            $aliases = $this->getAliasesFields();
            $user = $this->getBy(['id' => $id], null, null, $recursive);
            return $user;
        }
    }

    /**
     * Get user by login
     * @access protected
     * @param string $user
     * @return array
     */
    public function login_getByLogin($user) {
        if (!empty($user)) {
            $aliases = $this->getAliasesFields();
            $loginField = $aliases['user'];
            $emailField = $aliases['email'];
            $docField = $aliases['doc'];
            
            $conditions = ["LOWER($loginField) LIKE '" . strtolower($user) . "' OR LOWER($emailField) LIKE '" . strtolower($user) . "' OR LOWER($docField) LIKE '" . strtolower($user) . "'", 'status'=>$this->getAttr('statusValue')];
            $recursive = \doctrine\Dashes\HASMANY;
            return $this->login_formatUser($this->getBy($conditions, null, null, $recursive));
        }
    }
    
    public function login_getByToken($token) {
        $tokenClass = $this->getAttr('foreignKeys')['token']['model'];
        $model = new $tokenClass();
        $token = $model->exchangeFieldsNames($token);
        return $this->login_formatUser($this->getById($token['user_id']));
    }
    
    public function login_formatUser($userData) {
        $result = [];
        
        foreach($userData as $fieldAlias => $fieldValue)
            if(in_array($fieldAlias, ['id', 'user', 'name', 'email', 'pass', 'is_admin']))
                $result[$fieldAlias] = $fieldValue;
        
        
        $userData = $this->captureAclProfileList($userData);
        $userData['acl'] = $this->filterAcl($userData['acl']);
        $result['profile'] = !empty($userData['acl']) ? array_map(function($item) { return $item['profile']; }, $userData['acl']) : [];
        $result['profileId'] = array_map(function($item) {
            return $item['id'];
        }, (array) @$result['profile']);
        
        $result['other'] = array_filter($userData, function($item){ return !is_array($item); });
        
        return $result;
    }
    
    public function formatSystemUrl($url, $token=false) {
        $tokenClass = $this->getAttr('foreignKeys')['token']['model'];
        $model = new $tokenClass();
        return $model->formatSystemUrl($url, $token);
    }

    public function getListByACL($criteria=[], $fields = null, $acl = [], $sys = null, $recursive = -1) {
//        $aliases = $this->getAliasesFields();
        $shield = \HiMax\Core::getInstance();
        empty($sys) && $sys = $shield->getData('system')['id'];

        if (!empty($acl)) {
            $models = \HiMax\Core::getMe()->getModels();
            $modelAcl = $models['acl'];
            $modelAccess = $models['access'];
            $modelAction = $models['action'];


            $actionCriteria = [];
            $actionCriteria['system_id'] = $sys;
            !empty($acl['module']) && ($actionCriteria['module'] = $acl['module']);
            !empty($acl['class']) && ($actionCriteria['class'] = $acl['class']);
            !empty($acl['action']) && ($actionCriteria['action'] = $acl['action']);

            $action = $modelAction->getData($actionCriteria['class'], $actionCriteria['action'], $actionCriteria['module'], (string) @$actionCriteria['route'], $actionCriteria['system_id']);
            if (empty($action)) {
                throw new \Exception('Action not found');
            }
            $actionIdList = \Crush\Collection::transform($action, '', ['id'], ['flatten']);

            $pnaccessPerfis = $modelAccess->find(['action_id' => $actionIdList], null, null, ['profile_id'], null, -1);
            $perfisIds = \Crush\Collection::transform($pnaccessPerfis, '', ['profile_id'], ['flatten']);
            $usuariosIds = \Crush\Collection::transform($modelAcl->find(['profile_id' => $perfisIds]), '', ['user_id'], ['flatten']);

            if (empty($usuariosIds)) {
                $usuariosIds = ['0'];
            }
            $criteria[$this->getAttr('primaryKey')] = $usuariosIds;
        }
        $list = array_map(function($item) {
            return array_change_key_case($item, CASE_LOWER);
        }, (array) $this->findActive($criteria, null, null, (array) $fields, null, $recursive));
        return $list;
    }

}
