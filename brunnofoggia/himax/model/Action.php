<?php

namespace HiMax\model;

class Action {
    
    use \doctrine\Dashes\Model {
        \doctrine\Dashes\Model::create as _create;
        \doctrine\Dashes\Model::findAll as _findAll;
    }
    protected $modelAttrDefaults = [
        'table' => \HiMax\TABLE_NAME_ACTION,
        'deactivate' => \HiMax\TABLES_FIELD_DELETE,
        'SystemAclControl' => false,
        'ignoreSystemFilter' => true,
        'access_public' => \HiMax\Core::ACTION_PUBLIC,
        'access_internal' => \HiMax\Core::ACTION_INTERNAL,
        'access_private' => \HiMax\Core::ACTION_PRIVATE,
        'foreignKeys' => [
            'system' => [
                'type' => \doctrine\Dashes\BELONGSTO,
                'key' => 'system_id',
                'model' => '\HiMax\model\System'
            ],
        ]
    ];

    public function findAll($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $systemFk = $this->getAttr('foreignKeys')['system'];
        $systemModel = $this->loadFkModel('system');
        $systemData = \HiMax\Core::getMe()->getData('system');
        if(!empty($systemData) && !empty($this->getAttr('SystemAclControl')) && empty($this->getAttr('ignoreSystemFilter'))) {
            $conditions[$systemFk['key']] = $systemData[$systemModel->getAttr('primaryKey')];
        }
        
        return $this->_findAll($conditions, $limit, $page, $columns, $orderby, $recursive);
    }
    
    public function create($data) {
        $systemModel = $this->loadFkModel('system');
        $systemData = \HiMax\Core::getMe()->getData('system');
        $systemId = @$systemData[$systemModel->getAttr('primaryKey')];
        
        $this->getAttr('SystemAclControl') && empty($data['system_id']) && !empty($systemId) && ($data['system_id'] = $systemId);
        
        return $this->_create($data);
    }
    
    
    
    public function getData($controller, $action, $module, $route = null, $sys = null) {
        $conditions = [];
        $this->getAttr('SystemAclControl') && !empty($sys) && ($conditions['system_id'] = $sys);
        
        /* find all actions for given system */
        $columns = ['id', 'security_level', 'module', 'class', 'action', 'url'];
        $this->getAttr('SystemAclControl') && ($columns[] = 'system_id');
        $list = $this->find($conditions, null, null, $columns);
        
        /* filter the one asked through action rules (str match or regexp) */
        $callback = function($item) use($controller, $action, $module, $route) {
            return \HiMax\Core::filterAction($item, $controller, $action, $module, $route);
        };
        
        $results = array_filter($list, $callback);
        return $results;
    }
    
    public function getAcl($conditions = []) {
        $columns = ['module', 'class', 'action', 'url', 'security_level', 'id'];
        $this->getAttr('SystemAclControl') && ($columns[] = 'system_id');
        $actionList = $this->find($conditions, null, null, $columns, [$this->getFieldName('module') .' DESC', $this->getFieldName('class') . ' DESC', $this->getFieldName('action') . ' DESC']);
        return $actionList;
    }
    
    public function getInternalAcl($sys='') {
        $conditions = ['security_level'=>$this->getAttr('access_internal')];
        $this->getAttr('SystemAclControl') && ($conditions['system_id'] = $sys);
        return $this->getAcl($conditions);
    }
    
    public function getPublicAcl($sys='') {
        $conditions = ['security_level'=>$this->getAttr('access_public')];
        $this->getAttr('SystemAclControl') && ($conditions['system_id'] = $sys);
        return $this->getAcl($conditions);
    }
    
    public function getPrivateAcl($actionIdList, $sys='') {
        $conditions = ['security_level'=>$this->getAttr('access_private')];
        $this->getAttr('SystemAclControl') && ($conditions['system_id'] = $sys);
        $conditions = array_merge([$this->getAttr('primaryKey') => $actionIdList], $conditions);
        
        return $this->getAcl($conditions);
    }
    
    public function getSecurityLevelList() {
        return [
            $this->getAttr('access_public') => (string) \HiMax\Core::ACTION_PUBLIC,
            $this->getAttr('access_internal') => (string) \HiMax\Core::ACTION_INTERNAL,
            $this->getAttr('access_private') => (string) \HiMax\Core::ACTION_PRIVATE,
        ];
    }
    
    public function checkAccessValue($action, $comparison) {
        $securityLevel = array_flip($this->getSecurityLevelList());
        return (string) $action['security_level'] === $securityLevel[(string) $comparison];
    }
}
