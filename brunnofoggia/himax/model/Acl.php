<?php

namespace HiMax\model;

class Acl {
    
    use \doctrine\Dashes\Model;
    protected $modelAttrDefaults = [
        'table' => \HiMax\TABLE_NAME_ACL,
        'deactivate' => \HiMax\TABLES_FIELD_DELETE,
        'foreignKeys' => [
            'profile' => [
                'type' => \doctrine\Dashes\BELONGSTO,
                'key' => 'profile_id',
                'model' => '\HiMax\model\Profile'
            ],
        ],
    ];
    
}
