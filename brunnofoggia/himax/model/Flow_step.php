<?php

namespace HiMax\model;

class Flow_step {
    use \doctrine\Dashes\Model;
    protected $modelAttrDefaults = [
        'primaryKey' => 'uniqid',
        'table' => \HiMax\TABLE_NAME_FLOW_STEP,
//        'recursive' => \doctrine\Dashes\HASMANY,
        'foreignKeys' => [
            'flow' => [
                'type' => \doctrine\Dashes\BELONGSTO,
                'key' => 'flow_uniqid',
                'model' => '\HiMax\model\Flow'
            ],
            'child' => [
                'type' => \doctrine\Dashes\HASMANY,
                'key' => 'flow_step_uniqid',
                'model' => '\HiMax\model\Flow_step_child'
            ],
        ]
    ];
    
    public function getNextSteps($id) {
        $conditions[] = "(SELECT COUNT(id) FROM flow_step_child WHERE flow_step_uniqid = '".$id."' AND flow_step_uniqid_child = uniqid) = 1";
        
        return $this->find($conditions);
    }
    
    public function getPrevStep($id) {
        $conditions[] = "(SELECT COUNT(id) FROM flow_step_child WHERE flow_step_uniqid_child = '".$id."' AND flow_step_uniqid = uniqid) > 0";
        return $this->find($conditions);
    }
}
