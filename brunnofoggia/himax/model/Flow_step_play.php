<?php

namespace HiMax\model;

class Flow_step_play {

    use \doctrine\Dashes\Model;

    protected $modelAttrDefaults = [
//        'primaryKey' => 'flow_step_uniqid',
        'table' => \HiMax\TABLE_NAME_FLOW_STEP_PLAY,
        'deactivate' => \DB_FIELD_DELETE,
//        'recursive' => \doctrine\Dashes\HASMANY,
        'foreignKeys' => [
            'step' => [
                'type' => \doctrine\Dashes\BELONGSTO,
                'key' => 'flow_step_uniqid',
                'model' => '\HiMax\model\Flow_step'
            ],
//            'child_tree' => [
//                'type' => \doctrine\Dashes\BELONGSTO,
//                'key' => 'flow_step_child_id',
//                'model' => '\HiMax\model\Flow_step_child'
//            ],
        ]
    ];

    public function getCurrentStepPlay($id, $table) {
        if (empty($id)) {
            return [];
        }

        $tableData = ['table' => $table, 'table_id' => $id];

        $data = $this->getBy($tableData, null, ['sys_created DESC']);

        return $data;
    }

    public function getPrevStepPlay($id, $table) {
        $currStepPlay = $this->getCurrentStepPlay($id, $table);

        if (!empty($currStepPlay['flow_step_child_id'])) {
            return $this->getStepPlay($id, $table, $currStepPlay['flow_step_child_id']);
        }

//        $allStepPlay = $this->findStepPlay($id, $table);
//        
//        if(count($allStepPlay)>1) {
//            array_pop($allStepPlay);
//            return array_pop($allStepPlay);
//        }
//        
//        return [];
    }

    public function getNextSteps($id, $table) {
        $currStepPlay = $this->getCurrentStepPlay($id, $table);

        if (!empty($currStepPlay['flow_step_child_id'])) {
            $stepModel = $this->loadFkModel('step');
            $nextSteps = $stepModel->getNextSteps($currStepPlay['flow_step_uniqid']);
            return \Crush\Collection::transform($nextSteps, '', ['uniqid'], ['flatten']);
        }

        return [];
    }

    public function getStepPlay($id, $table, $stepPlayId) {
        if (empty($id)) {
            return [];
        }

        $tableData = ['table' => $table, 'table_id' => $id, 'id' => $stepPlayId];

        $data = $this->find($tableData, null, null, null, ['sys_created ASC']);

        return !empty($data) ? array_shift($data) : null;
    }

    public function findStepPlay($id, $table) {
        if (empty($id)) {
            return [];
        }

        $tableData = ['table' => $table, 'table_id' => $id];

        $data = $this->find($tableData, null, null, null, ['sys_created ASC']);

        return $data;
    }

    public function gotoNext($id) {
        $arguments = func_get_args();
        $table = $arguments[count($arguments) == 2 ? 1 : 2];
        $stepId = count($arguments) == 3 ? $arguments[1] : NULL;

        $current = $this->getCurrentStepPlay($id, $table);
//        printf("<pre>%s</pre>\n<br>", var_export($current, true));
        $stepModel = $this->loadFkModel('step');
        $nextSteps = $stepModel->getNextSteps($current['flow_step_uniqid']);

        if (empty($stepId) && !empty($nextSteps)) {
            $nextStep = array_shift($nextSteps);
        } else if (!empty($stepId)) {
            $nextSteps = \Crush\Collection::transform($nextSteps, 'uniqid');
            $nextStep = @$nextSteps[$stepId];
        }

        if (!empty($nextStep)) {
            $flowPlayData = ['table' => $table, 'table_id' => $id];
            $flowPlayData['flow_step_uniqid'] = $nextStep['uniqid'];
            $flowPlayData['flow_step_child_id'] = $current['id'];
            $flowPlayData['sys_user'] = @\HiMax\Core::getMe()->getData('user')['id'];
            $flowPlayData['comment'] = (string) @$_GET['comment'];

            return $this->create($flowPlayData);
        }
    }

    public function goBack($id) {
        $arguments = func_get_args();
        $table = $arguments[count($arguments) == 2 ? 1 : 2];
        $stepBackId = count($arguments) == 3 ? $arguments[1] : NULL;

        // check if there is 2 step plays
        $list = $this->findStepPlay($id, $table);
        if (count($list) < 2) {
            return;
        }

        // check "goback to"
        if (!empty($stepBackId)) {
            $stepTo = $list[count($list) - 2];
            if (strpos($stepTo['flow_step_uniqid'], $stepBackId) === false) {
                return;
            }
        }

        // go back
        $current = $this->getCurrentStepPlay($id, $table);
        $this->remove($current['id']);

        // set comment_back
        $current = $this->getCurrentStepPlay($id, $table);
        if (!empty(@$_GET['comment'])) {
            $this->update($current['id'], ['comment_back' => (string) @$_GET['comment']]);
        }

        return $current['id'];
    }

}
