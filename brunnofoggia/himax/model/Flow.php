<?php

namespace HiMax\model;

class Flow {
    use \doctrine\Dashes\Model;
    protected $modelAttrDefaults = [
        'primaryKey' => 'uniqid',
        'table' => \HiMax\TABLE_NAME_FLOW,
//        'recursive' => \doctrine\Dashes\HASMANY,
        'foreignKeys' => [
            'step' => [
                'type' => \doctrine\Dashes\HASMANY,
                'key' => 'flow_uniqid',
                'model' => '\HiMax\model\Flow_step'
            ],
        ]
    ];
    
    public function getSteps($flowId) {
        $stepModel = $this->loadFkModel('step');
        $conditions = ['flow_uniqid'=>$flowId];
        
        return $stepModel->find($conditions);
    }
    
    public function getInitialSteps($flowId) {
        $stepModel = $this->loadFkModel('step');
        $conditions = ['flow_uniqid'=>$flowId];
        $conditions[] = '(SELECT COUNT(id) FROM flow_step_child WHERE flow_step_uniqid_child = uniqid) = 0';
        
        return $stepModel->find($conditions);
    }
}
