<?php

namespace HiMax;

!defined(__NAMESPACE__ . '\TABLE_NAME_CONTROL_RECORD') 
        && define(__NAMESPACE__ . '\TABLE_NAME_CONTROL_RECORD', constant(__NAMESPACE__ . '\TABLES_PREFFIX') . 'record');
!defined(__NAMESPACE__ . '\TABLE_NAME_CONTROL_RECORD_SHARE') 
        && define(__NAMESPACE__ . '\TABLE_NAME_CONTROL_RECORD_SHARE', constant(__NAMESPACE__ . '\TABLES_PREFFIX') . 'record_share');
/* SHARE */
!defined(__NAMESPACE__ . '\RECORDCONTROL_OWNER') && define(__NAMESPACE__ . '\RECORDCONTROL_OWNER', 0);
!defined(__NAMESPACE__ . '\RECORDCONTROL_GROUP') && define(__NAMESPACE__ . '\RECORDCONTROL_GROUP', 1);
!defined(__NAMESPACE__ . '\RECORDCONTROL_PUBLIC') && define(__NAMESPACE__ . '\RECORDCONTROL_PUBLIC', 2);
!defined(__NAMESPACE__ . '\RECORDCONTROL_USER') && define(__NAMESPACE__ . '\RECORDCONTROL_USER', 3);
!defined(__NAMESPACE__ . '\RECORDCONTROL_DEPARTMENT') && define(__NAMESPACE__ . '\RECORDCONTROL_DEPARTMENT', 4);
/* ACCESS */
!defined(__NAMESPACE__ . '\RECORDCONTROL_READ') && define(__NAMESPACE__ . '\RECORDCONTROL_READ', 4);
!defined(__NAMESPACE__ . '\RECORDCONTROL_WRITE') && define(__NAMESPACE__ . '\RECORDCONTROL_WRITE', 2);
!defined(__NAMESPACE__ . '\RECORDCONTROL_EXECUTE') && define(__NAMESPACE__ . '\RECORDCONTROL_EXECUTE', 1);
/**
 * HiMax
 *
 * A toolkit implement to authenticate and authorize users actions
 *
 * @package     HiMax
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/himax
 */


trait ControlRecord {
    
    /**
     * Defines enumerator of share type
     * @return array
     */
    public function getRecordControlShareList() {
        return [
            RECORDCONTROL_OWNER => 'owner',
            RECORDCONTROL_GROUP => 'group',
            RECORDCONTROL_PUBLIC => 'public',
            RECORDCONTROL_USER => 'user',
            RECORDCONTROL_DEPARTMENT => 'department',
        ];
    }
    
    /**
     * Defines enumerator of access
     * @return array
     */
    public function getRecordControlAccessList() {
        return [
            RECORDCONTROL_READ => 'read',
            RECORDCONTROL_WRITE => 'write',
            RECORDCONTROL_EXECUTE => 'execute',
        ];
    }
    
    /**
     * Defines what are the default options for defining security of a record on simple tab
     * @return array
     */
    public function getRecordControlDefaultLists() {
        return [
            'site' => [
                'guest' => 'Guest',
                'logged' => 'Authenticated User',
            ],
            'record' => [
                'my_profile' => 'Anyone in my group',
                'my_department' => 'Anyone in my department',
                'public' => 'Anyone',
                'private' => 'Just me',
            ],
        ];
    }
    
    /**
     * Retrive list of default list options
     * Used on simple tab of security management
     * @return array
     */
    public function findRecordControlDefaultRecords() {
        $fullAccess = \HiMax\RECORDCONTROL_READ+\HiMax\RECORDCONTROL_WRITE+\HiMax\RECORDCONTROL_EXECUTE;
        $records = [
            'site' => [
                'guest' => [
                    [
                        'share' => \HiMax\RECORDCONTROL_PUBLIC,
                        'access' => \HiMax\RECORDCONTROL_EXECUTE,
                    ],
                ],
                'logged' => [
                    [
                        'share' => \HiMax\RECORDCONTROL_USER,
                        'access' => \HiMax\RECORDCONTROL_EXECUTE,
                    ],
                ],
            ],
            'record' => [
                'my_profile' => [
                    [
                        'share' => \HiMax\RECORDCONTROL_GROUP,
                        'share_id' => 'my_profile',
                        'access' => $fullAccess,
                    ],
                ],
                'my_department' => [
                    [
                        'share' => \HiMax\RECORDCONTROL_DEPARTMENT,
                        'share_id' => 'my_department',
                        'access' => $fullAccess,
                    ],
                ],
                'public' => [
                    [
                        'share' => \HiMax\RECORDCONTROL_PUBLIC,
                        'access' => $fullAccess,
                    ],
                ],
                'private' => [
                ],
            ],
        ];
        
        return $records;
    }
    
    /**
     * Applies record control events to the model that requires it
     * @example description Should be applied before parent events on constructor
     * @return type
     */
    public function getRecordControlEvents() {
        $events = [];
        
        $afterSavePersistAction = function($event, &$data, $id) {
            if(!empty($data['control_record'])) {
                if(!$this->canManageRow($id)) {
                    unset($data['control_record']);
                    return;
                }
                
                $controlRecordModel = $this->loadModel('\HiMax\model\Control_record');
                
                $shareData = ['table'=>$this->getAttr('table'), 'table_id'=>$data[$this->getAttr('primaryKey')]];
                if(empty($data['control_record']['owner'])) { unset($data['control_record']['owner']); }
                if(empty($data['control_record']['group'])) { unset($data['control_record']['group']); }
                
                $share = array_merge($controlRecordModel->touch($shareData), $data['control_record']);
                $share['_default_records'] = $this->getRecordControlDefaultRecords((array)@$share['_default']);
                
                !empty($data['control_record']['share']) && ($share['share'] = $data['control_record']['share']);
                $controlRecordModel->update($share[$controlRecordModel->getAttr('primaryKey')], $share);
            }
        };
        
        $afterFindAllAction = function($event, &$results) {            
            foreach($results as $x => $y) {
                if(!empty($y[$this->getAttr('primaryKey')]) && 
                        !$this->canAccessRow($y[$this->getAttr('primaryKey')], $this->getRowAction())) {
                    unset($results[$x]);
                }
            }
            sort($results);
            $this->setRowAction('currentAction', RECORDCONTROL_READ);
        };
                
        $beforeUpdate = function ($event, &$data, &$id) {
            if(!$this->canAccessRow($id, RECORDCONTROL_WRITE)) {
                $data = NULL;
                $id = NULL;
            }
        };
        
        $beforeDelete = function ($event, &$id) {
            if(!$this->canAccessRow($id, RECORDCONTROL_WRITE)) {
                $id = NULL;
            }
        };
        
        $events[] = [['after', 'create'], $afterSavePersistAction];
        $events[] = [['after', 'update'], $afterSavePersistAction];
        $events[] = [['after', 'findAll'], $afterFindAllAction];
        $events[] = [['before', 'update'], $beforeUpdate];
        $events[] = [['before', 'delete'], $beforeDelete];
        
        return $events;
    }
    
    
    /**
     * Get All record control data for a record
     * @param mixed $id
     * @return array
     */
    protected $recordControlRows = [];
    public function getRecordControlRows($id=NULL) {
        if(empty($id)) { return []; }
        if(!empty($this->recordControlRows[$id])) { return $this->recordControlRows[$id]; }
        
        $controlRecordModel = $this->loadModel('\HiMax\model\Control_record');
        $shareData = ['table'=>$this->getAttr('table'), 'table_id'=>$id];

        $data = $controlRecordModel->getBy($shareData, null, null, \doctrine\Dashes\HASMANY);
        !empty($data) && ($data['_default'] = $controlRecordModel->explodeDefaultList($data['default']));
        if(!empty($data) && !empty($data['share']))
            foreach($data['share'] as $x => $y) 
                $data['share'][$x] = $this->readRecordControlAccess($y);

        $this->recordControlRows[$id] = $data;
        
        return $data;
    }
    
    /**
     * Split access number into list of options of access set
     * @param type $data
     * @return type
     */
    public function readRecordControlAccess($data) {
        $access = $data['access'] = (int) $data['access'];
        $accessList = $this->getRecordControlAccessList();
        
        $accessFoundList = [];
        foreach($accessList as $a => $b)
            if($access >= (int) $a) {
                $access -= $a;
                $accessFoundList[] = $a;
            }
        
        $data['_access'] = $accessFoundList;
        return $data;
    }
    
    /**
     * Replace default keys per default records
     * Used on simple tab of security management
     * @param type $defaultList
     * @return array
     */
    public function getRecordControlDefaultRecords($defaultList) {
        $results = [];
        $allRecords = $this->findRecordControlDefaultRecords();
        if(empty($defaultList)) return $results;
        
        foreach($defaultList as $group => $list) {
            !is_array($list) && ($list = [$list]); // makes able to have multiple options of default record per type defined
            foreach($list as $item) {
                $records = $allRecords[$group][$item];
                $results = array_merge($results, $records);
}
            
            $results = $this->replaceDefaultFlags($results);
        }
        return $results;
    }
    
    /**
     * Replace default record flags
     * @param array $results
     * @return array
     */
    protected function replaceDefaultFlags($results) {
        if(empty($results)) { return $results; }
        
        foreach($results as $x => $y) {
            $my_profile = \HiMax\Core::getMe()->getMainProfile();
            $my_department = \HiMax\Core::getMe()->getMainDepartment();
            
            if(!empty($y['share_id']) && in_array($y['share_id'], ['my_profile', 'my_department'])) {
                $results[$x]['share_id'] = ${$y['share_id']};
            }
        }
        
        return $results;
    }
    
    /**
     * Check if logged user can do the given action on the given record
     * @param type $id
     * @param type $action
     * @return boolean
     */
    public function canAccessRow($id, $action=RECORDCONTROL_READ) {
        $recordControlData = $this->getRecordControlRows($id);
        if(empty($recordControlData['id']) || $this->canManageRow($id)) return true;
        
        if(!empty($recordControlData['share'])) {
            foreach($recordControlData['share'] as $recordControlShare) {
                if(!in_array($action, $recordControlShare['_access'], true)) continue;

                $method = 'canAccessRowMatch_'.$this->getRecordControlShareList()[$recordControlShare['share']];
                if($this->{$method}($recordControlShare)) {
                    return true;
                }
            }
        }
        return false;
    }
    
    /**
     * Check if share data given is set as public
     * @param array $share
     * @return boolean
     */
    protected function canAccessRowMatch_public($share) {
        return true;
    }
    
    /**
     * Check if share data given matches with logged user
     * @param array $share
     * @return boolean
     */
    protected function canAccessRowMatch_user($share) {
        if(\HiMax\Core::getMe()->isLogged()===true && (empty($share['share_id']) || (string) $share['share_id'] === (string) \HiMax\Core::getMe()->getData('user')['id'])) return true;
        return false;
    }
    
    /**
     * Check if share data given matches with logged user profile
     * @param array $share
     * @return boolean
     */
    protected function canAccessRowMatch_group($share) {
        if(empty($share['share_id']) || (\HiMax\Core::getMe()->userHasProfile($share['share_id']) || $this->isHigherGroupThan($share['share_id']))) return true;
        return false;
    }
    
    /**
     * Check if share data given matches with logged user department
     * @param array $share
     * @return boolean
     */
    protected function canAccessRowMatch_department($share) {
        if(empty($share['share_id']) || (\HiMax\Core::getMe()->userHasDepartment($share['share_id']))) return true;
        return false;
    }
    
    /**
     * Check if logged user can manage permissions of the given record
     * @param mixed $id
     * @return boolean
     */
    public function canManageRow($id) {
        if(empty($id)) return true;
        return $this->isUserOwner($id) || $this->isHigherUserOnSameGroupThanOwner($id) || $this->isHigherGroupThanOwner($id);
    }
    
    /**
     * Check if logged user is the owner
     * @param mixed $id
     * @return boolean
     */
    public function isUserOwner($id) {
        $recordControlData = $this->getRecordControlRows($id);
        $loggedUserInfo = \HiMax\Core::getMe()->getData('user');
        
        return empty($recordControlData) || empty($recordControlData['owner']) || (\HiMax\Core::getMe()->isLogged()===true && (string) $loggedUserInfo['user'] === (string) $recordControlData['owner']);
    }
    
    /**
     * Check if logged user belongs to the profile owner of the record
     * @param mixed $id
     * @return boolean
     */
    public function isGroupOwner($id) {
        $recordControlData = $this->getRecordControlRows($id);
        
        if(empty($recordControlData['group'])) return $this->isUserOwner($id);
        return \HiMax\Core::getMe()->isGroupOwner($recordControlData['group']);
    }
    
    /**
     * Verifies if logged user belongs to a profile that is on a higher level (is_admin) than the profile owner of the record
     * Users on a higher level can do anything to the record
     * @param integer $id
     * @return boolean
     */
    public function isHigherGroupThanOwner($id) {
        $recordControlData = $this->getRecordControlRows($id);
        
        if(empty($recordControlData['group'])) { return false; }
        return $this->isHigherGroupThan($recordControlData['group']);
    }
    
    /**
     * Verifies if logged user belongs to the same profile as the record and if logged user is on a higher level (is_admin) than the record owner
     * Users on a higher level can do anything to the record
     * @param integer $id
     * @return boolean
     */
    public function isHigherUserOnSameGroupThanOwner($id) {
        $recordControlData = $this->getRecordControlRows($id);
        return \HiMax\Core::getMe()->isHigherUserOnSameGroupThan($recordControlData['owner'], $recordControlData['group']);
    }
    
    /**
     * Verifies if logged user belongs to a profile that is on a higher level (is_admin) than the provided
     * Users if a higher level can do anything to the record
     * @param integer $id
     * @return boolean
     */
    public function isHigherGroupThan($group) {
        $groups = !empty(\HiMax\Core::getMe()->getModels()['profile']) ? \Crush\Collection::transform(\HiMax\Core::getMe()->getModels()['profile']->findActive([], NULL, NULL, NULL, NULL, -1), 'id') : [];
        $recordGroupInfo = !empty($groups[$group]) ? $groups[$group] : NULL;
        
        if(empty($recordGroupInfo)) { return false; }
        return \HiMax\Core::getMe()->isHigherGroupThanLevel($recordGroupInfo['is_admin']);
    }
    
    /**
     * Define an action for the next find
     * @param integer $action
     * @return object this
     */
    public function setRowAction($action) {
        if(in_array((int) $action, [RECORDCONTROL_READ, RECORDCONTROL_WRITE, RECORDCONTROL_EXECUTE])) {
            $this->setAttr('currentAction', $action);
        }
        return $this;
    }
    
    /**
     * Get next action set for find
     * @return integer
     */
    public function getRowAction() {
        if(empty($this->getAttr('currentAction'))) {
            $this->setRowAction(RECORDCONTROL_READ);
        }
        
        return $this->getAttr('currentAction');
    }
    
    /**
     * Shortcut to set execute action for next find
     * @return type
     */
    public function setRowActionExecute() {
        return $this->setRowAction(RECORDCONTROL_EXECUTE);
    }

    /**
     * Set to what relations the row action set should be fowarded to
     * @param integer $recursive
     */
    public function setRowActionRecursive($recursive = null) {
        $this->setAttr('rowActionRecursive', $recursive);
        
        return $this;
    }
    
    /**
     * Foward current row action to relations according to param rowActionRecursive that indicates what models will receive by relation type
     * @param object $model
     * @param integer $relationType
     */
    public function fowardRowAction($model, $relationType) {
        $rowActionRecursive = (int) $this->getAttr('rowActionRecursive');
        if($rowActionRecursive >= $relationType) {
            $model->setRowAction($this->getAttr('currentAction'))->setRowActionRecursive($this->getAttr('rowActionRecursive'));
        }
        
        return $this;
    }
    
}
