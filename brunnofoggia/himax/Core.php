<?php

namespace HiMax;

!defined(__NAMESPACE__ . '\TABLES_FIELD_DELETE') && define(__NAMESPACE__ . '\TABLES_FIELD_DELETE', 'sys_delet');
!defined(__NAMESPACE__ . '\TABLES_FIELD_STATUS') && define(__NAMESPACE__ . '\TABLES_FIELD_STATUS', 'status');
!defined(__NAMESPACE__ . '\TABLES_PREFFIX') && define(__NAMESPACE__ . '\TABLES_PREFFIX', 'control_');

/**
 * HiMax
 *
 * A toolkit implement to authenticate and authorize users actions
 *
 * @package     HiMax
 * @category	Security
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/himax
 */
class Core {
    use \Singleton,
            \DarkTrait {
        \DarkTrait::getAttr as _getAttr;
        \DarkTrait::setAttr as _setAttr;
    }
    
    /** login objects */
    protected static $login = [];
    public $frameworkInstance = null;
    public $frameworkBridge = null;
    
    
    const CONTROL_NONE = 0;
    /** implements authentication security */
    const CONTROL_AUTH = 1;
    /** implements authentication & access control list security */
    const CONTROL_ACL = 2;

    
    /** action types */
    const ACTION_PRIVATE = 2;
    const ACTION_INTERNAL = 1;
    const ACTION_PUBLIC = 0;
    
    /** error codes */
    const ERROR_TOKENNOTFOUND = 498;
    const ERROR_USERNOTFOUND = 1;
    const ERROR_INVALIDPASSWORD = 401;
    const ERROR_NOTAUTHENTICATED = 440;
    const ERROR_FORBIDDEN = 403;
    const ERROR_INTERNAL = 500;
    const ERROR_CONFIGDATANOTFOUND = 2;
    
    const SESSIONPREFFIX = 'himax-core';
    
    public function __call($name, $arguments) {
        return !empty(static::$login['control']) && method_exists(static::$login['control'], $name) ? 
            call_user_func_array([static::$login['control'], $name], (array) @$arguments) : NULL;
    }
    
    /**
     * Authenticate, check access and if needed throw error
     * @access public
     */
    public function run() {
        static::getFrameworkBridge();
        $this->setAttr('log', $log = \Crush\Log::get('himax'));
        $log->clearOldFiles(10, 'i');
        $controlClass = !empty($_SESSION['config']['controlLogin']) ? $_SESSION['config']['controlLogin'] : '\HiMax\GenericShield';
        if(!$this->getAttr('initialized') && !empty($controlClass)) {
            $control = static::$login['control'] = new $controlClass();
            $control->setAttr('log', $log);
        }
        
        $auth = $this->authorize();
        return $auth;
    }

    /**
     * Format route to predefined pattern
     * @param string $route
     * @return string
     */
    public static function formatRoute($route = null) {
        $routeRegexp = '/^\/?(\w+)?(\/\w+)?(\/\w+)?.*/';
        if (!empty($route)) {
            $route = preg_replace($routeRegexp, '$1$2$3', $route);
        }

        return $route;
    }
    
    public static function getFrameworkInstance() {
        if(function_exists('\get_instance')) {
            return \get_instance();
        }
        global $app;
        if(empty($app)) {
            throw new \Exception('function get_instance or global variable named "app" need to communicate with the framework');
        }
        return $app;
    }
    
    public static function getFrameworkBridge() {
        $self = static::getMe();
        
        if(empty($self->frameworkBridge)) {
            $frameworkInstance = static::getFrameworkInstance();
            $class = '\HiMax\framework\\';
            switch (true) {
                case class_exists('CI_Controller', false):
                    $class .= 'Codeigniter';
                    break;
                default:
                    $class .= 'Generic';
                    break;
            }
            $frameworkBridge = new $class;

            $self->frameworkInstance = $frameworkInstance;
            $self->frameworkBridge = $frameworkBridge;
        }
    
        return [$self->frameworkInstance, $self->frameworkBridge];
    }
    
    public static function filterAction($item, $controller, $action, $module, $route) {
        $item['url'] = \HiMax\Core::formatRoute($item['url']);

        return  (
            static::matchActionData($controller, $item['class'])
            &&
            static::matchActionData($action, $item['action'])
            &&
            ($module === null || static::matchActionData($module, $item['module']) )
            )
            ||
            ($route !== null && !empty($item['url']) && $item['url'] === $route);
    }
    
    public static function matchActionData($testData, $actionData) {
        return ($actionData === $testData || (\Crush\Basic::detectRegexp($actionData) && preg_match($actionData, $testData)));
    }
    
    public function getAttr($name, $forceDefault = false) {
        return $this->_getAttr($name, $forceDefault);
    }
    
    public function setAttr($name, $value) {
        return $this->_setAttr($name, $value);
    }
    
    public function authorize($output=true) {
        $auth = static::$login['control']->authorize();

        if($auth!==true && !empty($_GET['debug_himax_output'])) {
            printf("<pre>%s</pre>\n", var_export($this->getAttr('log')->readContent(), true));die;
        }
        
        $output && $this->output($auth);
        return $auth;
    }

    public function checkInstances($callback, $initialValue = false) {
        $value = $initialValue;
        foreach(static::$login as $login) {
            $result = call_user_func($callback, $login);
            if($result !== $value) {
                $value = $result;
                break;
}
        }
        return $value;
    }
    
    /**
     * Can be used to check if an attempt of login ocurred, or if authentication was invalid, etc.
     * @param type $info
     * @return type
     */
    public function testInfo($info) {
        $callback = function($login) use ($info) {
            return !empty($login->info[$info]);
        };
        return $this->checkInstances($callback);
    }
    
    /**
     * Can be used to get info about an attempt of login ocurred, or if authentication was invalid, etc.
     * @param type $info
     * @return type
     */
    public function getInfo($info) {
        $callback = function($login) use ($info) {
            return !empty($login->info[$info]) ? $login->info[$info] : null;
        };
        return $this->checkInstances($callback, null);
    }

}
