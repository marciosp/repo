<?php

namespace Crush;

/**
 * Upload
 * 
 * 
 *
 * @package     Crush
 * @category	Utilities
 * @author Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/crush
 */
class Upload {

    protected static $baseFolder = 'upload';
    protected static $basePath = null;
    protected static $targetPath = null;

    const ERR_UNKNOWN = 0;
    const ERR_FILETYPE = 2;
    const ERR_FILESIZE = 3;
    
    protected static $uploadInfo = [];

    /**
     * Set base path for storage files
     * @param string $baseFolder
     * @param string $basePath
     * @throws \Exception
     */
    public static function set($baseFolder = '', $basePath = '') {
        if (!empty($baseFolder))
            static::$baseFolder = $baseFolder;
        
        if (!empty($basePath))
            $basePath = [$basePath . static::$baseFolder];
        else
            $basePath = [__DIR__ . '/../../../' . static::$baseFolder, static::$baseFolder];
        
        foreach($basePath as $x) {
            if (is_dir($x) && is_writable($x)) {
                return static::$basePath = $x;
            }
        }
        
        throw new \Exception('Base Path does not exists. ( ' . $basePath[0] . ' )');
    }

    /**
     * In case of receiving dir name try to create and set it as writable for storage files
     * @throws \Exception
     */
    public static function prepareFolder($dir='') {
        static::$targetPath = static::$basePath;
        empty($dir) && !empty(@$_REQUEST['upload']['dir']) && ($dir = @$_REQUEST['upload']['dir']);
        if (empty($dir))
            return;

        $targetFolder = rtrim($dir, '/'); // Relative to the root
        $targetPath = static::$basePath . '/' . $targetFolder;

        if (!is_dir($targetPath) && !(mkdir($targetPath) && (is_writable($targetPath) || chmod($targetPath, 777)))) {
            throw new \Exception('Target Path does not exists and could not be created or set as writable. ( ' . $targetPath . ' )');
        }

        static::$targetPath = $targetPath;
    }

    /**
     * In case of receiving file types check if it matches with target file
     * @return boolean
     */
    public static function checkFileType($file) {
        if(empty($_REQUEST['upload']) || empty($_REQUEST['upload']['types'])) return true;
        
        $typesAccepted = (array) explode(',', (string) @$_REQUEST['upload']['types']); // File extensions
        $fileInfo = pathinfo($file['name']);
        if (empty($typesAccepted) || in_array(strtolower((string) @$fileInfo['extension']), $typesAccepted)) {
            return true;
        }
        return false;
    }

    /**
     * In case of receiving file types check if it matches with target file
     * @return boolean
     */
    public static function checkFileSize($file) {
        if(empty($_REQUEST['upload']) || empty($_REQUEST['upload']['size'])) return true;
        
        $limitInfo = explode(' ', (string) @$_REQUEST['upload']['size']);
        $sizeLimit = (floatval($limitInfo[0]));
        empty($unit) && ($unit = 'MB');
        $filesize = filesize($file['tmp_name']);
        $size = static::convertSize($filesize, @$limitInfo[1]);
        
        if($size > $sizeLimit) {
            return false;
        }
        return true;
    }
    
    /**
     * Convert byte size to other unit
     * @param float $size in bytes
     * @param string $unit
     * @return float
     */
    public static function convertSize($size, $unit = 'MB') {
        if (!$size) return $size;

        $bytes = floatval($size);
        $unit = strtoupper($unit);
        $units = [
            'TB' => pow(1024, 4),
            'GB' => pow(1024, 3),
            'MB' => pow(1024, 2),
            'KB' => 1024,
            'B' => 1
        ];

        $math = $bytes / $units[$unit];
//        $result = str_replace(".", ",", strval(round($math, 2)));
        
        return $math;
    }

    /**
     * Store first uploaded file of $_FILES list
     * @return boolean
     * @throws Exception
     */
    public static function uploadFile() {        
        if (!static::$basePath)
            static::set();
        static::prepareFolder();

        if (empty($_FILES)) {
            throw new Exception('No file received');
        }
        
        $file = array_shift($_FILES);
        $tmpFile = $file['tmp_name'];
        $fileInfo = pathinfo($tmpFile);
        $fileName = empty(@$_REQUEST['upload']['name']) ? $file['name'] : $_REQUEST['upload']['name'].preg_replace('/.*(\.\w{1,4})$/', '$1', $file['name']);
        $targetFile = date('YmdHis') . '_' . str_replace(' ', '_', $fileName);
        $targetFilePath = static::$targetPath . '/' . $targetFile;

        $error = null;
        if (!static::checkFileSize($file))
            $error = ['status' => static::ERR_FILESIZE, 'data' => ['targetFilePath' => str_replace(static::$basePath, '', $targetFilePath)]];
        else if (!static::checkFileType($file))
            $error = ['status' => static::ERR_FILETYPE, 'data' => ['targetFilePath' => str_replace(static::$basePath, '', $targetFilePath)]];
        else if (!@move_uploaded_file($tmpFile, $targetFilePath))
            $error = ['status' => static::ERR_UNKNOWN, 'data' => ['targetFilePath' => str_replace(static::$basePath, '', $targetFilePath)]];
        
        static::$uploadInfo[] = !$error ? ['status' => 1, 'data' => ['filename' => $targetFile, 'filepath' => str_replace(static::$basePath, '', $targetFilePath)]] : $error;
        return !$error;
    }
    
    /**
     * Get information of the upload process
     * @return type
     */
    public static function getUploadInfo() {
        return static::$uploadInfo;
    }
    
    /**
     * Get List of files
     * @return array
     */
    public static function getFiles($name='', $dir='') {
        empty($name) && !empty(@$_REQUEST['upload']['name']) && ($name = @$_REQUEST['upload']['name']);
        if (!static::$basePath)
            static::set();
        static::prepareFolder($dir);
        
        $str = static::$targetPath . '/*' . (string) @$_REQUEST['upload']['name'] . '.*';
        $list = glob($str);
        
        if(!empty($list)) {
            foreach($list as $x => $item) {
                $list[$x] = str_replace(static::$basePath, '', $item);
            }
        }
        
        return $list;
    }
    
    /**
     * Get last file of the list
     * @return string
     */
    public static function getLastFile($name='', $dir='') {
        $list = static::getFiles($name, $dir);
        return !empty($list) ? array_pop($list) : '';
    }
    
    public static function getLastFileLink($name='', $dir='') {
        return static::formatAsLink(static::getLastFile($name, $dir));
    }
    
    public static function formatAsLink($filePath) {
        return $filePath ? '/' . static::$baseFolder . $filePath : '';
    }
    
    public static function content($filePath) {
        return file_get_contents(__DIR__ . '/../../../' . static::$baseFolder . $filePath);
    }

}
