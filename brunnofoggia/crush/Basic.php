<?php

namespace Crush;

/**
 * Basic
 *
 * Basic set of utilities
 *
 * @package     Crush
 * @category	Utilities
 * @author Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/crush
 */
class Basic {

    /**
     * Extract a class short name (name without namespace
     * @param type $class
     * @return type
     */
    public static function getClassShortName($class) {
        $name = !is_object($class) ? $class : get_class($class);
        return array_reverse(explode('\\', $name))[0];
    }

    /**
     * Alias for getClassShortName
     */
    public static function gcsn($class) { return static::getClassShortName($class); } 

    /**
     * Validate data using Illuminate\Validation
     * @param array $data
     * @param array $rules
     * @return object
     */
    public static function validate($data, $rules=[], $customValidators=[]) {
        $translation_file_loader = new \Illuminate\Translation\FileLoader(new \Illuminate\Filesystem\Filesystem, __DIR__ . '/lang');
        $translator = new \Illuminate\Translation\Translator($translation_file_loader, 'en');
        $validation_factory = new \Illuminate\Validation\Factory($translator, new \Illuminate\Container\Container);
        if(!empty($customValidators)) {
            foreach($customValidators as $validator) {
                $validation_factory->extend($validator[0], $validator[1], (!empty($validator[2]) ? $validator[2] : '') );
            }
        }
        $validator = !empty($rules) ? $validation_factory->make($data, $rules) : $validation_factory;

        return $validator;
    }

    /**
     * Checks if the content of a string is a regexp
     * @param string $str
     * @return bool
     */
    public static function detectRegexp($str) {
        return substr($str, 0, 1) === '/' && substr($str, strlen($str) - 1, 1) === '/';
    }

    /**
     * Test to see if a request contains the HTTP_X_REQUESTED_WITH header
     * @return bool
     */
    public static function isAjaxRequest() {
        return (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) === 'xmlhttprequest') || preg_match('/application\/json/', $_SERVER['HTTP_ACCEPT']);
    }

    public static $baseCharacters = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz";

    /**
     * Method to increment a alphanumeric string
     * @param type $x string that will be incremented
     * @param type $digits how many digits the string must contain
     * @return string
     */
    public static function increment($x, $digits) {
        $charList = static::$baseCharacters;

        empty($x) && $x = '0';
        $x = trim($x);

        // completa cadeia de caracteres com o numero de digitos parametrizados
        $y = strlen($x) < $digits ? str_pad($x, $digits, substr($charList, 0, 1), STR_PAD_LEFT) : $x;


        $result = preg_split("//", $y, -1, PREG_SPLIT_NO_EMPTY);
        // percorre a cadeia de caracteres de tras pra frente e incrementa o primeiro caractere possivel
        for ($i = $digits - 1; $i >= 0; --$i) {
            $char = $result[$i];
            $currentChar = strpos($charList, $char);
            $nextCharPosition = $currentChar + 1;

            if ($nextCharPosition < strlen($charList)) {
                $nextChar = substr($charList, $nextCharPosition, 1);

                $result[$i] = $nextChar;
                if($i < $digits) for($j = $i+1; $j < $digits; ++$j)
                    $result[$j] = substr($charList, 0, 1);
                break;
            }
        }

        return implode('', $result);
    }

    /**
     * Method to decrement a alphanumeric string
     * @param type $x string that will be decremented
     * @param type $digits how many digits the string must contain
     * @return string
     */
    public static function decrement($x, $digits) {
        $charList = static::$baseCharacters;

        // completa cadeia de caracteres com o numero de digitos parametrizados
        if (strlen($x) < $digits) {
            $x = str_pad($x, $digits, substr($charList, 0, 1), STR_PAD_LEFT);
        }

        $result = preg_split("//", $x, -1, PREG_SPLIT_NO_EMPTY);
        // percorre a cadeia de caracteres de tras pra frente e decrementa o primeiro caractere possivel
        for ($i = $digits - 1; $i >= 0; --$i) {
            $char = $result[$i];
            $currentChar = strpos($charList, $char);
            $previousCharPosition = $currentChar - 1;

            if ($previousCharPosition > -1) {
                $previousChar = substr($charList, $previousCharPosition, 1);

                $result[$i] = $previousChar;

                // define os caracteres apos o decrementado para o ultimo caractere. ex: 3[00] > 2[99]
                for ($j = $i + 1; $j < $digits && $i < $digits - 1; ++$j)
                    $result[$j] = substr($charList, strlen($charList) - 1, 1);
                break;
            }
        }

        return implode('', $result);
    }
    
    public static function data2uri($file, $mime)
    {
        if(!is_file($file)) return false;
        
        $contents = file_get_contents($file);
        $base64   = base64_encode($contents); 
        return ('data:' . $mime . ';base64,' . $base64);
    }

}
