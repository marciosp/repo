<?php

namespace Crush;
empty(date_default_timezone_get()) && date_default_timezone_set('America/Sao_Paulo');

!defined('Crush\REQUESTID') && define('Crush\REQUESTID', uniqid());

/**
 * Log
 * @author Bruno Foggia
 */
class Log {

    protected static $instance;

    protected $dir = 'tmp/log/';

    protected $logName;
    protected $id;
    protected $filePath;

    /**
     * Helps to have one global log acessible through all classes executed in one request and also to have differente instances
     * @param type $logId will help you to identify your log file and have an specific instance for your log
     * @param type $dir directory to store log file
     * @param type $inputRequestData defines if request info will be appended to log file
     * @param type $holdInstance
     * @return type
     */
    public static function get($logName='', $dir=NULL, $inputRequestData = true, $holdInstance = NULL) {
        if (!static::$instance || !empty($logName)) {
            $instance = new static($logName, $dir, $inputRequestData);
            if($holdInstance || (empty($logName) && empty(static::$instance) && $holdInstance!==false)) { // will store a global instance unless you have set a custom id for yout log or set holdinstance param as false
                static::$instance = $instance;
            }
        } else {
            $instance = self::$instance;
        }
        return $instance;
    }

    public static function getExistent() {
        if (!static::$instance) return NULL;
        return static::$instance;
    }

    /**
     * 
     * @param type $customId Let you specify a name to be part of filename
     * @param type $dir directory to store log file
     * @param type $inputRequestData defines if request info will be appended to log file
     */
    public function __construct($logName = '', $dir=NULL, $inputRequestData = true) {
        !empty($dir) && ($this->dir = $dir);
        $this->id = $this->generateId($logName);
        $this->createFile();
        if ($inputRequestData) {
            $this->inputRequestData();
        }
    }
    
    /**
     * 
     * @param type $howLongIsValid how many days, months or years back it should consider not old enough to erase
     * @param type $numberFlag defines what the first variable relates to. Years, months, days, hours, Years & months, Years & months & days, etc.
     */
    public function clearOldFiles($howLongIsValid, $numberFlag='m') {
        $dateFlag = ['Y'=>'years','m'=>'months','d'=>'days','H'=>'hours','i'=>'minutes'];
        if(!array_key_exists($numberFlag, $dateFlag)) {
            throw new \Exception('Specify a valid date flag');
        }
        $dateStr = explode($numberFlag, 'YmdHi')[0] . $numberFlag;
        
        $dateList = [];
        $currentFlag = $dateFlag[$numberFlag];
        $strFrom = '-'.$howLongIsValid.' '.$currentFlag;
//        $dateList[] = $dateFrom = date($dateStr,strtotime($strFrom));
        
        $i = ((int) $howLongIsValid)-1;
        $done = false;
        while(!$done) {
            $strTo = '-'.($i--).' '.$currentFlag;
            $dateTo = date($dateStr,strtotime($strTo));
            if((int)$dateTo>(int)date($dateStr)) {
                break;
            }
            $dateList[] = $dateTo;
        }
        
        if(!empty($dateList)) {
            $params = ['date'=>'(?!('.implode('|', $dateList).'))\d*'];
            !empty($this->logName) && ($params['custom'] = $this->logName);
            $files = $this->listOfFiles($params);
            $dir = $this->getDir();
            if(!empty($files)) foreach($files as $file) unlink($dir.$file);
        }
        
        return $this;
    }

    /**
     * Add a string line to log
     * @param string $text
     * @param type $breakLine
     * @return $this
     */
    public function add($text='', $breakLine = 1) {
        if(!is_string($text) && !is_int($text)) {
            $text = var_export($text, true);
        }
        
        if (!empty($breakLine)) {
            $text .= str_repeat("\n", $breakLine);
        }

        file_put_contents($this->filePath, $text, FILE_APPEND);
        return $this;
    }

    /**
     * Read content from log
     * @return string
     */
    public function readContent() {
        if(is_file($this->getPath())) {
            return @file_get_contents($this->getPath());
        }
    }

    /**
     * Get a list of files from current set dir considering params x name composition
     * @param array $params
     * @return array
     */
    public function listOfFiles(Array $params = array()) {
        $namePattern = '/^' . $this->buildName($params, ('\w*')) . '$/';
        $dir = $this->getDir();

        $files = array();
        if ($handle = opendir($dir)) {
            while (false !== ($entry = readdir($handle))) {
                if (preg_match($namePattern, $entry)) {
                    $files[] = $entry;
                }
            }
            closedir($handle);
        }

        return $files;
    }

    /**
     * Get request info to save on log file
     */
    public function inputRequestData() {
        $data = array();
        $data[] = 'File: ' . (string) @$_SERVER['SCRIPT_FILENAME'];
        $data[] = 'Url: ' . (string) @$_SERVER['REQUEST_URI'];
        $data[] = 'Method: ' . (string) @$_SERVER['REQUEST_METHOD'];
        $data[] = 'Date: ' . date('Y-m-d H:i:s');
        $data[] = 'Remote Ip: ' . (string) @$_SERVER['REMOTE_ADDR'];
        $data[] = 'Remote Port: ' . (string) @$_SERVER['REMOTE_PORT'];
        $data[] = 'Authentication User: ' . (string) @$_SERVER['PHP_AUTH_USER'];
        $data[] = "---\n";

        $this->add(implode("\n", $data));
    }
    
    protected function getBaseAbsolutePath() {
        return __DIR__ . '/../../../' ;
    }

    public function getDir() {
        if(!preg_match('/\/$/', $this->dir))
            $this->dir .= '/';
        return $this->getBaseAbsolutePath() . $this->dir;
    }
    
    public function getRelativeDir() {
        return '/' . $this->dir;
    }

    protected function createDir($dirRelativePath, $dirFullPath) {
        $basePath = $this->getBaseAbsolutePath();
        $dirRelativePath = explode('/', $dirRelativePath);
        
        $path = $basePath;
        foreach($dirRelativePath as $folder) { if(!empty($folder)) {
            $path .= $folder;
            if (!is_dir($path)) {
                @mkdir($path);
                @chmod($path, 0775);
            }
            
            $path .= '/';
        }}
    }

    public function readFiles(Array $params) {
        $files = $this->listOfFiles($params);
        $dir = $this->getDir();

        $data = array();
        foreach ($files as $file) {
            $data[] = array($file, file_get_contents($dir . $file));
        }

        return $data;
    }

    public function readFile($file) {
        $dir = $this->getDir();
        return array(str_replace($dir, '', $file), file_get_contents(is_file($dir . $file) ? $dir . $file : $file));
    }

    public function generateId($logName = '') {
        if(is_string($logName) && strpos($logName, '#')===0) { // this allows to have uniq files with the exact name inputed
            return substr($logName, 1);
        }
        
        $data = !is_array($logName) ? [] : $logName;
        
        if(!is_array($logName)) {
            $data['request_id'] = \Crush\REQUESTID;
            $data['id'] = uniqid();
            $data['ip'] = (string) @$_SERVER['REMOTE_ADDR'];
            $data['date'] = date('YmdHis');
            $data['user'] = (string) @$_SERVER['PHP_AUTH_USER'];
            $data['custom'] = (string) $logName;

            !empty($data['custom']) && ($this->logName = $data['custom']);
            return $this->buildName($data);
        }
        return $this->buildName($data, NULL);
    }

    public function buildName(Array $param, $pad = '') {
        $data = [];
        $data[] = isset($param['date']) ? $param['date'] : $pad;
        $data[] = isset($param['custom']) ? $param['custom'] : $pad;
        $data[] = isset($param['ip']) ? str_replace('.', '_', $param['ip']) : $pad;
        $data[] = isset($param['user']) ? $param['user'] : $pad;
        $data[] = isset($param['request_id']) ? $param['request_id'] : $pad;
        $data[] = isset($param['id']) ? $param['id'] : $pad;
        
        $data = array_filter($data, function($item) { return $item !==NULL; });
        
        return implode('-', $data);
    }

    public function createFile() {
        $dirPath = $this->getDir();
        $this->createDir($this->getRelativeDir(), $dirPath);
        $this->filePath = $dirPath . $this->id;
    }

    public function clearFile() {
        file_put_contents($this->filePath, '');
        return $this;
    }
    
    public function getId() {
        return $this->id;
    }
    
    public function getPath() {
        return $this->filePath;
    }
    
    public function getLink() {
        return $this->getRelativeDir() . $this->getId();
    }
    
}
