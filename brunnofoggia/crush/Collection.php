<?php

namespace Crush;

/**
 * Collection
 *
 * 
 *
 * @package     Crush
 * @category	Utilities
 * @author Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/crush
 */
class Collection {

    /**
     * Transforms a list according to the settings given
     * @param array $data
     * @param string $field1 name of the key field
     * @param array $field2 field name list to the results
     * @param mixed $options
     * @return array
     */
    public static function transform($data, $field1, $field2 = [], $options = null) {
        if (!is_array($data)) {
            return [];
        }
        !is_array($options) && $options = array($options);

        $list = [];
        $header = [];
        foreach ($data as $index1 => $row) {
            $value1 = (!empty($field1) || $field1 === 0) && $field1 !== '=' ? static::_getListValue($field1, $row, $options) : ($field1 === '=' ? $index1 : $field1);
            is_array($field2) && count($field2) === 0 ? ($field2 = array_keys($row)) : null;

            if (is_array($field2)) {
                foreach ($field2 as $field) {
                    $field = explode(' ', $field, 2);
                    $value = static::_getListValue($field[0], $row, (array) $options);
                    $alias = (count($field) > 1) ? $field[1] : $field[0];

                    $value2[$alias] = static::_filterValues($field, $value, $options);

                    !in_array($alias, $header) && $header[] = $alias;
                }
            }

            $result = static::_applyItemFilters($value2, $options);

            if (in_array('removeEmpty', $options) && empty($result)) {
                continue;
            }

            $key = (!empty($value1) || (string) $value1 === '0') ? $value1 : count($list);
            if ((!is_int($key) || !empty($field1)) && in_array('array', $options)) {
                !array_key_exists($key, $list) && ($list[$key] = []);
                $key = static::_applyKeyFormat($key, $result, $options);
                $list[$key][] = $result;
            } else {
                $key = static::_applyKeyFormat($key, $result, $options);
                $list[$key] = $result;
            }
        }

        return static::_applyListFilters($list, $header, $options);
    }

    /**
     * Allows to format key of list
     * @param type $key
     * @param type $result
     * @param type $options
     * @return string
     */
    protected static function _applyKeyFormat($key, $result, $options) {
        if(array_key_exists('keyFormat', $options)) {
            $key = $options['keyFormat']($key, $result);
        }
        if(array_key_exists('keyPad', $options)) {
            list($length, $string, $type) = $options['keyPad'];
            $key = preg_replace_callback('/[a-zA-Z0-9]+/', function($x) use($length, $string, $type) {
                return str_pad($x[0], $length, $string, $type);
            }, $key);
        }
        
        return $key;
    }

    /**
     * 
     */
    protected static function _filterValues($field, $value, Array $options) {
        if (in_array('trim', $options)) {
            if (!is_array($value))
                $value = trim($value);
            else
                foreach ($value as $x => $y)
                    $value[$x] = trim($y);
        }

        if (array_key_exists('lists', $options) && !empty($options['lists']) && !is_array($value) && is_array($field) && !empty($options['lists'][$field[0]])) {
            $value = $options['lists'][$field[0]][$value];
        }

        if (array_key_exists('callbacks', $options) && !empty($options['callbacks']) && !is_array($value) && is_array($field) && !empty($options['callbacks'][$field[0]])) {
            $value = $options['callbacks'][$field[0]]($value);
        }

        return $value;
    }

    /**
     * 
     */
    protected static function _applyItemFilters($result, Array $options) {
        if (in_array('arrayValues', $options) && is_array($result)) {
            $result = array_values($result);
        }

        if (in_array('flatten', $options) && is_array($result)) {
            $result = array_shift($result);
        }

        if (in_array('csv', ($options), true) && is_array($result)) {
            $result = implode(';', $result);
        }

        return $result;
    }

    /**
     * 
     */
    protected static function _applyListFilters($list, $header, Array $options) {
        if (in_array('header', $options)) {
            array_unshift($list, $header);
        }
        if (in_array('header', array_keys($options), true)) {
            $header = array_map(function ($item) use ($options) {
                return in_array($item, array_keys($options['header']), true) ? $options['header'][$item] : $item;
            }, $header);

            if (in_array('csv', ($options), true)) {
                $header = implode(';', $header);
            }

            array_unshift($list, $header);
        }
        if (in_array('interlace', $options, true)) {
            $nlist = [];
            foreach ($list as $x => $item) {
                if (!is_array($item))
                    $nlist[] = $item;
                else
                    (!empty($item) && ($nlist = static::interlace($nlist, $item)));
            }
            $list = $nlist;
        }
        if (in_array('nolevels', $options)) {
            $list = static::nolevels($list);
        }

        if (in_array('csv', ($options), true)) {
            $list = implode("\n", $list);
        }

        return $list;
    }

    /**
     * 
     */
    protected static function _getListValue($key, $data, Array $options) {
        if (is_array($key) || is_object($key)) {
            return $key;
        } elseif (empty($key) && (string)$key!=='0') {
            return $data;
        }

        $keys = explode('+', $key);
        $keysValues = [];
        foreach ($keys as $key) {
            $fields = strlen($key)===1 && $key==='.' ? ['.'] : explode('.', $key);
            $extractedData = (array) $data;
            foreach ($fields as $x => $field) {

                if ($field === '[]' && is_array($extractedData) && !array_key_exists($field, $extractedData)) {
                    return static::transform($extractedData, '', [implode('.', array_slice($fields, $x + 1))], (array) @$options['under']);
                } else if (!is_array($extractedData) || !array_key_exists($field, $extractedData)) {
                    $extractedData = $field;
                }

                $extractedData = is_array($extractedData) ? $extractedData[$field] : $extractedData;
                !is_array($extractedData) && ($keysValues[] = $extractedData);
            }
        }

        return count($keys) > 1 && !empty($keysValues) ? implode('', $keysValues) : $extractedData;
    }

    /**
     * pass any number of arrays and it will interlace and key them properly
     * @example . arr1 = [1, 2, 3], arr2 = [4, 5, 6], result = [1, 4, 2, 5, 3, 6]
     * @reference http://php.net/manual/pt_BR/function.array-merge.php#85029
     * @return boolean
     */
    public static function interlace() {
        $args = func_get_args();
        $total = count($args);

        if ($total < 2) {
            return FALSE;
        }

        $i = 0;
        $j = 0;
        $arr = array();

        foreach ($args as $arg) {
            foreach ($arg as $v) {
                $arr[$j] = $v;
                $j += $total;
            }

            $i++;
            $j = $i;
        }

        ksort($arr);
        return array_values($arr);
    }

    /**
     * pass any number of arrays and it will unify them into one
     * @example . arr1 = [1, 2, 3], arr2 = [4, 5, 6], result = [1, 2, 3, 4, 5, 6]
     * @return boolean
     */
    public static function unify() {
        $args = func_get_args();
        $total = count($args);

        if ($total < 2) {
            return FALSE;
        }

        $i = 0;
        $arr = [];

        foreach ($args as $arg) {
            foreach ($arg as $v) {
                $arr[$i++] = $v;
            }
        }

        return $arr;
    }

    /**
     * Array Filter
     * Need to have an array filter with values and keys for PHP older than 5.6
     */
    public static function filter($list, $callback) {
        foreach ($list as $key => $value) {
            if (!$callback($value, $key)) {
                unset($list[$key]);
            }
        }

        return $list;
    }

    /**
     * Creats an array tree from an array list crossing idfield and parentidfield 
     * as reference to differ root elements from child elements
     * 
     * @param array $flat flat array list
     * @param type $idField
     * @param type $parentIdField
     * @param type $childNodesField array key to store the list of children found for each root
     * @return array tree
     */
    public static function convertToTree(array $flat, $idField = 'id', $parentIdField = 'parent', $childNodesField = 'children') {
        $indexed = array();
        // first pass - get the array indexed by the primary id  
        foreach ($flat as $row) {
            $indexed[$row[$idField]] = $row;
            $indexed[$row[$idField]][$childNodesField] = array();
        }

        //second pass
        $root = null;
        foreach ($indexed as $id => $row) {
            $indexed[$row[$parentIdField]][$childNodesField][$id] = & $indexed[$id];
            if (!$row[$parentIdField]) {
                $roots[] = $id;
            }
        }
        foreach ($roots as $root) {
            $tree[$root] = $indexed[$root];
        }
        return $tree;
    }

    /**
     * Converts a multidimensional array into a flat one
     * @param type $array
     * @return type
     */
    public static function array_tree_to_single(&$array) {
        $filtered = array();
        array_walk_recursive($array, function($val, $key) use(&$filtered) {
            $filtered[$key][] = $val;
        });
        foreach ($filtered as $key => $val) {
            foreach ($val as $k => $v) {
                $arr[$k][$key] = $v;
            }
        }
        return $arr;
    }

    public static function string2list($str, $splitter1=';', $splitter2='=') {
        $stack = array_map(function($item) use($splitter2) {
                return strpos($item, $splitter2)!==false ? explode($splitter2, $item, 2) : [$item, $item];
            }, explode($splitter1, $str));
            
        $stack = static::transform($stack, 0, [ '1'], ['flatten']);
        
        return $stack;
    }
    
    public static function nolevels($list, $key='') {
        $nlist = [];
        if(!empty($list)) {
            if(is_array($list)) {
                foreach($list as $x => $y) {
                    $key2 = !strlen($key) ? (string) $x : $key.'['.$x.']';
                    if(is_array($y)) {
                        $nlist = array_merge($nlist, static::nolevels($y, $key2));
                    } else {
                        $nlist[$key2] = $y;
                    }
                }
            } else {
                $nlist[$key] = $list;
            }
        }
        
        return $nlist;
    }

}
