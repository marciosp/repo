# Model

Properties avaiable for customization: table, primaryKey, foreignKeys[], fieldsFormat[], recursive

## Properties avaiable for customization
    
    class Sample_model extends CI_Model {
        protected $table = 'sample';
        // primaryKey column name
        protected $primaryKey = 'id';
        // recursive will tell what relational data to look for. "0" will bring nothing more than data for the according model.
        // And the other options for it are:
        // \doctrine\Dashes\BELONGSTO, \doctrine\Dashes\HASMANY and \doctrine\Dashes\HASANDBELONGSTOMANY
        protected $recursive = 0;
        // fieldsFormat helps to handle data before save
        protected $fieldsFormat = [
            // using regexp replacement
            'birthdate': ['/(\d{2})\/(\d{2})\/(\d{4})/', '$3-$2-$1'],
            // will let you create a method called format_slug to handle it's value
            'slug': ":"
        ];
        // foreignKeys will help saving related data
        // the key of the foreignKeys item is used to identify data send when saving a row using create/update
        // models are needed for every table because they will tell their info like primary key name, table name, foreign keys and more
        protected $foreignKeys = [
            // Sample of "Belongs To"
            'name1' => [
                'type' => \doctrine\Dashes\BELONGSTO,
                'key' => 'name1_id',
                'model' => 'Name1_model'
            ],
            // Sample of "Has Many"
            'name2' => [
                'type' => \doctrine\Dashes\HASMANY,
                'key' => 'sample_id',
                'model' => 'Name2_model'
            ],
            // Sample of "Has And Belongs To Many"
            'name3' => [
                'type' => \doctrine\Dashes\HASANDBELONGSTOMANY,
                'keys' => ['sample_id', 'name4_id'],
                'bridge' => 'Name3_model',
                'model' => 'Name4_model',
                'modelDataKey' => 'name4'
            ],
            
        ];
    }

## Default methods
- Persist methods - save, update, create, delete, deleteAll
- Retrieve methods - get, find, exists
- Persist related data methods - saveBelongsTo, saveHasMany, saveHasAndBelongsToMany
- Retrieve related data methods - getRelated, getBelongsTo, getHasMany, getHasAndBelongsToMany

This methods are available by default.
The descriptions about what they do and how they do it are described on their comments.

For more details take a little look at Model.php