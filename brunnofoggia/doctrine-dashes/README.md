# Dashes
[![Minimum PHP Version](http://img.shields.io/badge/php-%3E%3D%205.4-8892BF.svg)](https://php.net/)
[![License](https://poser.pugx.org/dereuromark/cakephp-tools/license.svg)](https://bitbucket.org/brunnofoggia/dashes)

This is an additional active record pack for DBAL Integration.
It will provide all is needed to connect with a database, read and persist relational data quick and easy

## Version notice

This works for **DBAL ^2.5**

## What this plugin provides?

### Features for Model
- Persist methods - save, update, create, delete, deleteAll
- Retrieve methods - get, find, exists
- Persist related data methods - saveBelongsTo, saveHasMany, saveHasAndBelongsToMany
- Retrieve related data methods - getRelated, getBelongsTo, getHasMany, getHasAndBelongsToMany
- Properties available for customization: table, primaryKey, foreignKeys[], fieldsFormat[], recursive

## Installation

You should install this with composer. Read the [VCS repo docs](https://getcomposer.org/doc/05-repositories.md#vcs).
A initial composer.json would looks like:

    {
      "require": {
          "doctrine/dbal": "^2.5",
          "brunnofoggia/doctrine-dashes": "@dev"
      }
    }

## Usage

1. Import it into classes that will use it

    class Sample_model {
        use \doctrine\Dashes\Model;
        protected $table = 'sample';
    }

2. Get an instance of the model

   2.1. Setting connection config before

        \Sample_model::setConnConfig('default', [
            'host' => 'localhost',
            'user' => 'root',
            'pass' => '123',
            'dbname' => 'test'
        ]);
        $test = new Sample_model;

    2.2. Sending connection config

        $test = new Sample_model([
            'host' => 'localhost',
            'user' => 'root',
            'pass' => '123',
            'dbname' => 'test'
        ]);

3. try to read a record

        $pk = 1;
        $test->get($pk);

### Detailed documentation
- [Model](Model.md)