<?php

/**
 * H-Basis Trait
 *
 * This is an additional pack for Models.
 * It will provide standards to read/persist data
 *
 * @package     H-Basis
 * @category	Utilities
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/h-basis
 */
define('HBasis\BELONGSTO', 1);
define('HBasis\HASMANY', 2);
define('HBasis\HASANDBELONGSTOMANY', 3);
define('HBasis\NORELATED', -1);

trait HBasis {

    use \DarkTrait,
        \Cake\Event\EventDispatcherTrait {
        \DarkTrait::getAttr as _getAttr;
        \DarkTrait::setAttr as _setAttr;
        \DarkTrait::explicitAttr as _explicitAttr;
    }

    /**
     * Defines default values for undeclared class properties
     * Array keys represents names of the possible properties that could be declared
     */
    protected $attrDefaults = [
        'primaryKey' => 'id',
        'foreignKeys' => [],
        'foreignKeysSeparator' => '',
        /*
         * This feature should be defined as an array where the keys represent fields name and values could be:
         * 1) regexp: an array with 2 items that are both regexp. The first will read the input and get the groups of values that will be used in the second to get the new format for the string
         * 2) method name: a string the refers to a method in this model to format the value
         * 3) default: string ":" for a method named starting with _ and proceeded with field name
         */
        'fieldsFormat' => [], // receives data about how to format inputted data before saving it
        'fieldsAliases' => [], // defines alias for inputted data
        'recursive' => \HBasis\BELONGSTO,
        'relatedRecursive' => \HBasis\NORELATED,
        'prepareRelatedDataMethodPrefix' => 'prepareRelatedData_',
        'findColumns' => '*',
        'deactivate' => false, // used to specify a column to be filled instead of deleting the record
        'deactivateValue' => '1',
        'status' => false,
        'statusValue' => '1',
        'activateValue' => '0',
        'transaction' => true,
        'DarkTraitGlobal' => '\HBasis',
        'setEvents' => true, // defines if observers will be applied and used
        'foreignDefaults' => true, // let you set a stack of default parameters to be sent when saving related records
    ];

    /**
     * Keep loaded models
     */
    public $model = [];

    public function __construct() {
        if($this->getAttr('setEvents')) {
        $this->_setEvents();
        $this->_applyEvents();
    }
    }
    
    public function _setEvents() {
        !$this->getAttr('events') && $this->setAttr('events', []);
        $this->explicitAttr('events');
        
        //
        $afterFindAll = function ($event, &$results) {
            $self = $this;
            $results = array_map(function($item) use($self){ return $self->exchangeFieldsNames($item); }, $results);
        };
        $this->events[] = [['after', 'findAll'], $afterFindAll];
        
        
        
        $beforeSave = function ($event, &$data) {
            $data = $this->saveBelongsTo($data);
            method_exists($this, 'beforeSave') && $this->beforeSave($event, $data);
        };
        $this->events[] = [['before', 'create'], $beforeSave];
        $this->events[] = [['before', 'update'], $beforeSave];
        
        
        $afterSave = function ($event, &$data, $id) {
            method_exists($this, 'afterSave') && $this->afterSave($event, $data, $id);
            method_exists($this, 'beforeSaveRelated') && $this->beforeSaveRelated($event, $data, $id);
            $this->saveHasMany($data);
            $this->saveHasAndBelongsToMany($data);
            method_exists($this, 'afterSaveRelated') && $this->afterSaveRelated($event, $data, $id);
        };
        
        $this->events[] = [['after', 'create'], $afterSave];
        $this->events[] = [['after', 'update'], $afterSave];
    }
    
    /**
     * Get a row by its primary key
     * @param id primaryKey to a spacific row
     * @param columns list of columns returned for the main row
     * @param recursive defines relations that will be covered by search
     * @access public
     */
    public function get($id, $columns = null, $recursive = null) {
        $recursive === null && $recursive = $this->getAttr('recursive');
        empty($columns) && $columns = $this->getAttr('findColumns');
        $result = count(($item = $this->find([$this->getAttr('primaryKey') => $id], 1, null, $columns, null, $recursive))) > 0 ? $item[0] : null;

        return $result;
    }

    /**
     * Get a row
     * @param id primaryKey to a spacific row
     * @param columns list of columns returned for the main row
     * @param recursive defines relations that will be covered by search
     * @access public
     */
    public function getBy($conditions = [], $columns = null, $orderby = null, $recursive = null) {
        $recursive === null && $recursive = $this->getAttr('recursive');
        empty($columns) && $columns = $this->getAttr('findColumns');
        return count(($item = $this->find($conditions, 1, null, $columns, $orderby, $recursive))) > 0 ? $item[0] : null;
    }

    /**
     * Find rows
     * @param conditions group of filters to results
     * @param columns list of columns returned for the main row
     * @param limit use it to paginate
     * @param page use it to paginate
     * @param recursive defines relations that will be covered by search
     * @access public
     */
    public function findAll($conditions = [], $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $this->dispatchEvent(['before', 'findAll'], ['conditions' => &$conditions,'limit' => &$limit,'page' => &$page,'columns' => &$columns,'orderby' => &$orderby,'recursive' => &$recursive,]);
        $query = $this->buildFind($conditions, $limit, $page, $columns, $orderby, $recursive);

        $results = $this->fetchAll($query);
        
        if(!empty($results) && is_array($results)) {
            $self = $this;
            $this->dispatchEvent(['after', 'findAll'], ['results' => &$results,]);
        }
        
        $results = $this->findRelated($results, $recursive);
        
        return $results;
    }

    /**
     * Find not deleted rows
     * @param conditions group of filters to results
     * @param columns list of columns returned for the main row
     * @param limit use it to paginate
     * @param page use it to paginate
     * @param recursive defines relations that will be covered by search
     * @access public
     */
    public function find($conditions = [], $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        if ($this->getAttr('deactivate') &&
                (count($conditions) !== 1 || !array_key_exists($this->getAttr('primaryKey'), $conditions)) // assure to retrieve belongsTo records
        ) {
            $conditions[] = $this->getAttr('deactivate') . ' <> ' . $this->quote($this->getAttr('deactivateValue'));
        }
        $results = $this->findAll($conditions, $limit, $page, $columns, $orderby, $recursive);

        return $results;
    }


    /**
     * Find not deleted and active rows
     * @param conditions group of filters to results
     * @param columns list of columns returned for the main row
     * @param limit use it to paginate
     * @param page use it to paginate
     * @param recursive defines relations that will be covered by search
     * @access public
     */
    public function findActive($conditions = array(), $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        !empty($this->getAttr('status')) && ($conditions[$this->getAttr('status')] = $this->getAttr('statusValue'));
        
        return $this->find($conditions, $limit, $page, $columns, $orderby, $recursive);
    }

    /**
     * Query
     * @param conditions group of filters to results
     * @param columns list of columns returned for the main row
     * @param limit use it to paginate
     * @param page use it to paginate
     * @param recursive defines relations that will be covered by search
     * @access public
     */
    public function buildFind($conditions = [], $limit = null, $page = null, $columns = null, $orderby = null, $recursive = null) {
        $recursive === null && $recursive = $this->getAttr('recursive');
        empty($columns) && $columns = $this->getAttr('findColumns');
        empty($limit) && $limit = $this->getAttr('findLimit');
        empty($page) && $page = 0;

        $columns = $this->exchangeListAliases($columns);
        
        $query = $this->getQueryBuilder();
        $query->select($columns)
                ->from($this->getAttr('table'));

        $this->where($query, $conditions);
        $this->orderby($query, $orderby);
        $this->limit($query, $limit, $page);

        return $query;
    }

    /**
     * Delete a row by its id
     * @param id primaryKey to a spacific row
     * @access public
     */
    public function delete($id) {
        $this->dispatchEvent(['before', 'delete'], ['id' => &$id,]);
        
        $query = $this->db->delete($this->getAttr('table'), [$this->getAttr('primaryKey') => $id]);
        $result = $this->affectedRows($query);
        
        $this->dispatchEvent(['after', 'delete'], ['id' => &$id,'result' => &$result,]);
        return $result;
    }

    /**
     * Delete rows
     * @param conditions group of filters
     * @access public
     */
    public function deleteAll($conditions = []) {
        $this->dispatchEvent(['before', 'deleteAll'], ['conditions' => &$conditions,]);
        
        $query = $this->_deleteAll($conditions);

        $result = $this->affectedRows($query);
        $this->dispatchEvent(['after', 'deleteAll'], ['result' => &$result,]);
        return $result;
    }

    /**
     * Used to detect if there if a column to set deactivated instead of delete the record
     * @param id primaryKey to a spacific row
     * @access public
     */
    public function remove($id) {
        $this->dispatchEvent(['before', 'remove'], ['id' => &$id,]);
        
        if ($this->getAttr('deactivate')) {
            $result = $this->update($id, [$this->getAttr('deactivate') => $this->getAttr('deactivateValue')]);
        } else {
            $result = $this->delete($id);
        }
        
        $this->dispatchEvent(['after', 'remove'], ['id' => &$id,'result' => &$result]);
        return $result;
    }

    /**
     * Alias for deleteAll (can be used to apply deactivate behavior)
     */
    public function removeAll($conditions = []) {
//        return $this->deleteAll($conditions);
        $this->dispatchEvent(['before', 'removeAll'], ['conditions' => &$conditions,]);
        
        if ($this->getAttr('deactivate')) {
            $result = $this->updateAll($conditions, [$this->getAttr('deactivate') => $this->getAttr('deactivateValue')]);
        } else {
            $result = $this->deleteAll($conditions);
        }

        $this->dispatchEvent(['after', 'removeAll'], ['result' => &$result]);
        return $result;
    }

    /**
     * Alias for deleteAll
     */
    public function deleteBy($conditions = []) {
        return $this->deleteAll($conditions);
    }

    /**
     * Save a row
     * @param data input to be persisted
     * @access public
     */
    public function save($data) {
        $operation = empty($data[$this->getAttr('primaryKey')]);
        return $operation ? $this->create($data) : $this->update($data[$this->getAttr('primaryKey')], $data);
    }

    /**
     * Save a list
     * @param type $list
     */
    public function saveEach($list) {
        if (empty($list)) {
            return NULL;
        }
        
        $fn = function($self, $list) {
            $result = true;
            foreach ($list as $item) {
                !$self->save($item) && ($result = false);
            }
            return $result;
        };
        
        $transaction = $this->getAttr('transaction');
        if (!$transaction) {
            return $fn($this, $list);
        }
        return $this->transact($fn, $this, $list);
    }

    /**
     * Create a row
     * @param data input to be persisted
     * @access public
     */
    public function create($data) {
        if (empty($data)) {
            return false;
        }

        $fn = function($self, $data) {
            $self->dispatchEvent(['before', 'create'], ['data' => &$data,]);
            $pk = $self->getAttr('primaryKey');
            
            $id = null;
            if(array_key_exists($pk, $data)) {
                if(empty($data[$pk])) unset($data[$pk]);
                else ($id = $data[$pk]);
            }
            
            $namedData = $self->exchangeFieldsAliases($data);
            $formattedData = $self->format($namedData);
            $result = $self->db->insert($this->getAttr('table'), $formattedData);
            if ($result) {
                $lastId = $self->lastInsertId($result);
                $data[$self->getAttr('primaryKey')] = $id = (!empty($lastId) ? $lastId : $id);
                
                $self->dispatchEvent(['after', 'create'], ['data' => &$data,'id' => &$id,]);
                
//                return $id ? $id : true;
                return $id;
            }
        };

        $transaction = $this->getAttr('transaction');
        if (!$transaction) {
            return $fn($this, $data);
        }
        return $this->transact($fn, $this, $data);
    }

    /**
     * Checks if a row exists for given id
     * @param id primaryKey to a spacific row
     * @access public
     */
    public function exists($id) {
        return !!$this->get($id, $this->getAttr('primaryKey'), \HBasis\NORELATED);
    }

    /**
     * Update a row
     * @param id primaryKey to a spacific row
     * @param data input to be persisted
     * @access public
     */
    public function update($id, $data) {
        if (empty($data)) {
            return false;
        }

        $fn = function($self, $id, $data) {
            $self->dispatchEvent(['before', 'update'], ['data' => &$data,'id' => &$id,]);
            $namedData = $self->exchangeFieldsAliases($data);
            $formattedData = $self->format($namedData);
            $result = !empty($formattedData) ? $self->db->update($self->getAttr('table'), $formattedData, [$self->getAttr('primaryKey') => $id]) : true;

            if ($result !== false) {
                $data[$self->getAttr('primaryKey')] = $id;
                
                $self->dispatchEvent(['after', 'update'], ['data' => &$data,'id' => &$id,]);
                return $id;
            }
        };

        $transaction = $this->getAttr('transaction');
        if (!$transaction) {
            return $fn($this, $id, $data);
        }
        return $this->transact($fn, $this, $id, $data);
    }

    /**
     * Runs update on data under conditions
     * @param conditions for update
     * @param data to be updated
     * @access public
     */
    public function updateAll($conditions, $data) {
        $this->dispatchEvent(['before', 'updateAll'], ['conditions' => &$conditions,'data' => &$data,]);
        $formattedData = $this->format($data);
        
        $result = $this->_updateAll($conditions, $formattedData);
        
        $this->dispatchEvent(['after', 'updateAll'], ['result' => &$result,]);
        return $result;
    }

    /**
     * Alias for updateAll
     */
    public function updateBy($conditions, $data) {
        return $this->updateAll($conditions, $data);
    }

    /**
     * Format data received before persist
     * @param data input to be formatted
     * @access protected
     */
    protected function format($data) {
        foreach ($this->getAttr('fieldsFormat') as $field => $format) {
            if (!is_array($data) || (is_array($format) && !array_key_exists($field, $data))) {
                continue;
            }

            switch (true) {
                case is_array($format):
                    $formatMethod = 'formatRegexp';
                    break;
                case preg_match('/^\:(\w*)/', $format, $matches):
                    empty($matches[1]) && $matches[1] = '_' . $field;
                    $formatMethod = 'format' . $matches[1];
                    break;
                default:
                    $formatMethod = $format;
                    break;
            }

            if (method_exists($this, $formatMethod)) {
                $value = $this->{$formatMethod}($field, @$data[$field], $format, $data);
//                (array_key_exists($field, $data) || $value !== false)
                ($value !== false) && ($data[$field] = $value);
            }
        }

        is_array($data) && ($data = array_filter($data, function($value) {
            return !is_array($value);
        }));

        return $this->quoteListKeys($data);
    }

    /**
     * Format value according to regular expressions received
     * @param field name of the field
     * @param value subject to format
     * @param regex rules (pattern and replacement) to apply
     * @param data entire row
     * @access protected
     */
    protected function formatRegexp($field, $value, $format, $data) {
        return preg_replace($format[0], $format[1], $value);
    }

    /**
     * Apply quotes in fields names to avoid problems with database
     * @param array $data
     * @return array
     */
    public function quoteListKeys($data) {
        if(!is_array($data)) return $data;
        
        $list = [];
        foreach ($data as $field => $value) {
            if (!is_int($field)) {
                $list[$this->quoteField($field)] = $value;
            } else {
                $list[$field] = $value;
            }
        }

        return $list;
    }

    /**
     * Splits field name from operator or ordenation info
     * @param type $field
     * @return type
     */
    public function extractFieldData($field) {
        if(is_string($field) && strpos($field, ' ')!==false) {
            $data = preg_replace('/\s{2,}/', ' ', trim($field));
            return array_pad(explode(' ', $data), 2, '');
        } elseif(is_string($field) && strpos($field, ' ')===false) {
            return [$field, ''];
        }
        return ['', ''];
    }
    
    /**
     * Get a flipped alias array where the aliases are the keys
     * @return array
     */
    public function getAliasesFields(){
        return is_array($this->getAttr('fieldsAliases')) ? array_flip($this->getAttr('fieldsAliases')) : [];
    }
    
    /**
     * Get the corresponding name of an alias
     * @param string $fieldAlias
     * @return string
     */
    public function getFieldName($fieldAlias) {
        $fieldName = $fieldAlias;
        $fieldAliasExtracted = $this->extractFieldData($fieldAlias)[0];
        if(!empty($fieldAliasExtracted)) {
            $aliases = $this->getAliasesFields();
            array_key_exists($fieldAliasExtracted, $aliases) && ($fieldName = $aliases[$fieldAliasExtracted]);
        }
        return $fieldName;
    }
    
    /**
     * Exchange fields alias to names
     * @param array $data retrieved data
     * @return array
     */
    public function exchangeFieldsAliases($data) {
        $aliases = $this->getAttr('fieldsAliases');
        $result = $data;
        if(!empty($aliases) && is_array($aliases)) {
            foreach($aliases as $field => $alias) {
                if(array_key_exists($alias, $data) && $alias !== $field) {
                    $result[$field] = $data[$alias];
                    unset($result[$alias]);
                }
            }
        }
        
        return $result;
    }
    
    /**
     * Exchange fields names to alias
     * @param array $data retrieved data
     * @return array
     */
    public function exchangeFieldsNames($data) {
        $aliases = $this->getAttr('fieldsAliases');
        $result = $data;
        if(!empty($aliases) && is_array($aliases)) {
            foreach($aliases as $field => $alias) {
                if(array_key_exists($field, $data) && $alias !== $field) {
                    $result[$alias] = $data[$field];
                    unset($result[$field]);
                }
            }
        }
        
        return $result;
    }
    
    public function exchangeListAliases($list) {
        $aliases = $this->getAliasesFields();
        if(!empty($aliases) && is_array($aliases)) {
            if(is_array($list) && !empty($list))
            foreach($list as $x => $alias) if(!is_array($alias) && array_key_exists($alias, $aliases)) {
                $list[$x] = $aliases[$alias];
            }
        }
        
        return $list;
    }

    /**
     * Persist related data
     * @param data input to be persisted
     * @access public
     */
    public function saveBelongsTo($data) {
        if(!is_array($data)) return $data;
        $relations = array_filter($this->getAttr('foreignKeys'), function($relation) {
            return $relation['type'] === \HBasis\BELONGSTO;
        });

        foreach ($relations as $foreignDataKey => $foreignConfig) {
            if (($foreignData = array_key_exists($foreignDataKey, $data) ? $data[$foreignDataKey] : null) === null) {
                continue;
            }

            $model = $this->loadModelInstance($foreignConfig['model']);

            $prepareRelatedDataMethodName = $this->getAttr('prepareRelatedDataMethodPrefix') . $foreignDataKey;
            method_exists($this, $prepareRelatedDataMethodName) && ($foreignData = $this->{$prepareRelatedDataMethodName}($foreignData, $data, $foreignConfig));

            $data[$foreignConfig['key']] = null;
            $foreignData = $this->setForeignDefaults($foreignConfig, $foreignData);
            if ($foreignKey = $model->save($foreignData)) {
                $data[$foreignConfig['key']] = $foreignKey;
            }
            unset($data[$foreignDataKey]);
        }

        return $data;
    }

    /**
     * Persist related data
     * @param data input to be persisted
     * @access public
     */
    public function saveHasMany($data) {
        if(!is_array($data)) return $data;
        $relations = array_filter($this->getAttr('foreignKeys'), function($relation) {
            return $relation['type'] === \HBasis\HASMANY;
        });

        foreach ($relations as $foreignDataKey => $foreignConfig) {
            $foreignData = array_key_exists($foreignDataKey, $data) ? $data[$foreignDataKey] : null;

            $model = $this->loadModelInstance($foreignConfig['model']);

            $prepareRelatedDataMethodName = $this->getAttr('prepareRelatedDataMethodPrefix') . $foreignDataKey;
            method_exists($this, $prepareRelatedDataMethodName) && ($foreignData = $this->{$prepareRelatedDataMethodName}($foreignData, $data, $foreignConfig));

            if (is_array($foreignData)) {
                foreach ($foreignData as $foreignDataItem) {
//                    printf("<pre>%s</pre>\n<br>", var_export('----------------', true));
//                    printf("<pre>%s</pre>\n<br>", var_export('----------------', true));
//                    printf("<pre>%s</pre>\n<br>", var_export('----------------', true));
//                    printf("<pre>%s</pre>\n<br>", var_export('----------------', true));
//                    printf("<pre>%s</pre>\n<br>", var_export($foreignDataItem, true));
//                    printf("<pre>%s</pre>\n<br>", var_export($foreignConfig['key'], true));
//                    printf("<pre>%s</pre>\n<br>", var_export($data[$this->getAttr('primaryKey')], true));
                    $foreignDataItem[$foreignConfig['key']] = $data[$this->getAttr('primaryKey')];
                    $foreignDataItem = $this->setForeignDefaults($foreignConfig, $foreignDataItem);
//                    printf("<pre>%s</pre>\n<br>", var_export($foreignDataItem, true));
                    $model->save($foreignDataItem);
                }
            }
            unset($data[$foreignDataKey]);
        }

        return $data;
    }

    /**
     * Persist related data
     * @param data input to be persisted
     * @access public
     */
    public function saveHasAndBelongsToMany($data) {
        if(!is_array($data)) return $data;
        $relations = array_filter($this->getAttr('foreignKeys'), function($relation) {
            return $relation['type'] === \HBasis\HASANDBELONGSTOMANY;
        });

        foreach ($relations as $foreignDataKey => $foreignConfig) {
            $foreignBridgeData = array_key_exists($foreignDataKey, $data) ? $data[$foreignDataKey] : [];

            if (array_key_exists('model', $foreignConfig)) {
                $model = $this->loadModelInstance($foreignConfig['model']);
                $bridge = $this->loadModelInstance($foreignConfig['bridge']);

                foreach ($foreignBridgeData as $foreignBridgeDataItem) {
                    if (($foreignData = array_key_exists($foreignConfig['modelDataKey'], $foreignBridgeDataItem) ? $foreignBridgeDataItem[$foreignConfig['modelDataKey']] : [])) {
                        if ($foreignKey = $model->save($foreignData)) {
                            $foreignBridgeDataItem[$foreignConfig['keys'][1]] = $foreignKey;
                        }
                        unset($foreignBridgeDataItem[$foreignConfig['modelDataKey']]);
                    }
                    $foreignBridgeDataItem[$foreignConfig['keys'][0]] = $data[$this->getAttr('primaryKey')];
                    $foreignBridgeDataItem = $this->setForeignDefaults($foreignConfig, $foreignBridgeDataItem);
                    $bridge->save($foreignBridgeDataItem);
                }
                unset($data[$foreignConfig['keys'][0]]);
            }
        }

        return $data;
    }

    /**
     * Set default foreign data to record if it exists and this functionallity is activated
     * @param type $foreignConfig
     * @param type $relatedData
     * @return array
     */
    public function setForeignDefaults($foreignConfig, $relatedData) {
        if($this->getAttr('foreignDefaults') && !empty($foreignConfig['default'])) {
            foreach($foreignConfig['default'] as $field => $value) if(!array_key_exists($field, $relatedData)) {
                $relatedData[$field] = $value;
            }
        }
        
        return $relatedData;
    }

    /**
     * Find all related data
     * @param data rows to find related data
     * @param recursive defines relations that will be covered by search
     * @param relatedNeedList filter the relations that will be collected
     * @access public
     */
    public function findRelated($results, $recursive = null, $relatedNeededList = []) {
        $recursive === null && $recursive = $this->getAttr('recursive');

        $this->dispatchEvent(['before', 'findRelated'], ['results' => &$results,'recursive'=>&$recursive]);

        if ($recursive && !empty($results)) {
            $recursive === null && $recursive = $this->getAttr('recursive');
            switch (true) {
                case $recursive >= \HBasis\HASANDBELONGSTOMANY:
                    foreach ($results as $x => $result) {
                        $results[$x] = $this->getHasAndBelongsToMany($results[$x], $relatedNeededList);
                    }
                case $recursive >= \HBasis\HASMANY:
                    $results = $this->findHasMany($results, $relatedNeededList);
                case $recursive >= \HBasis\BELONGSTO:
                    $results = $this->findBelongsTo($results, $relatedNeededList);
                default:
                    break;
            }
        }

        $this->dispatchEvent(['after', 'findRelated'], ['results' => &$results,'recursive'=>&$recursive]);

        return $results;
    }

    /**
     * Collect all related data
     * @param data row to get related data
     * @param recursive defines relations that will be covered by search
     * @param relatedNeedList filter the relations that will be collected
     * @access public
     */
    public function getRelated($data, $recursive = null, $relatedNeededList = []) {
        $recursive === null && $recursive = $this->getAttr('recursive');
        
        $this->dispatchEvent(['before', 'getRelated'], ['results' => &$data,'recursive'=>&$recursive]);
        switch (true) {
            case $recursive >= \HBasis\HASANDBELONGSTOMANY:
                $data = $this->getHasAndBelongsToMany($data, $relatedNeededList);
            case $recursive >= \HBasis\HASMANY:
                $data = $this->getHasMany($data, $relatedNeededList);
            case $recursive >= \HBasis\BELONGSTO:
                $data = $this->getBelongsTo($data, $relatedNeededList);
            default:
                break;
        }
        
        $this->dispatchEvent(['after', 'getRelated'], ['results' => &$data,'recursive'=>&$recursive]);
        return $data;
    }

    /**
     * Collect related data
     * @param data row to get related data
     * @param relatedNeedList filter the relations that will be collected
     * @access public
     */
    public function getBelongsTo($data, $relatedNeededList = []) {
        $relations = \Crush\Collection::filter($this->getAttr('foreignKeys'), function($relationData, $relationKey) use($relatedNeededList) {
                    return $relationData['type'] === \HBasis\BELONGSTO && (empty($relatedNeededList) || in_array($relationKey, $relatedNeededList, true));
                });

        foreach ($relations as $foreignDataKey => $foreignConfig) {
            $data[$foreignDataKey] = [];
            if (!($foreignKey = array_key_exists($foreignConfig['key'], $data) ? $data[$foreignConfig['key']] : null)) {
                continue;
            }

            $model = $this->loadModelInstance($foreignConfig['model']);
            $primaryKey = empty($foreignConfig['primaryKey']) ? $model->getAttr('primaryKey') : $foreignConfig['primaryKey'];
            $relatedRecursive = array_key_exists('recursive', $foreignConfig) ? $foreignConfig['recursive'] : $this->getAttr('relatedRecursive');

            if ($foreignData = $model->get($foreignKey, null, $relatedRecursive)) {
                $data[$foreignDataKey] = $foreignData;
            }
        }
        return $data;
    }

    /**
     * Collect related data for a list of results
     * @param results collection to get related data for
     * @param relatedNeedList filter the relations that will be collected
     * @access public
     */
    public function findBelongsTo($results, $relatedNeededList = []) {
        if (empty($results)) {
            return $results;
        }

        $relations = \Crush\Collection::filter($this->getAttr('foreignKeys'), function($relationData, $relationKey) use($relatedNeededList) {
                    return $relationData['type'] === \HBasis\BELONGSTO && (empty($relatedNeededList) || in_array($relationKey, $relatedNeededList, true));
                });

        foreach ($relations as $foreignDataKey => $foreignConfig) {
            $model = $this->loadModelInstance($foreignConfig['model']);
            method_exists($model, 'fowardRowAction') && $model->fowardRowAction($model, $foreignConfig['type']);

            $primaryKey = empty($foreignConfig['primaryKey']) ? $model->getAttr('primaryKey') : $foreignConfig['primaryKey'];
            $foreignIdList = \Crush\Collection::transform($results, '', [$foreignConfig['key']], ['flatten', 'removeEmpty']);

            if (empty($foreignIdList)) {
                continue;
            }

            $relatedRecursive = array_key_exists('recursive', $foreignConfig) ? $foreignConfig['recursive'] : $this->getAttr('relatedRecursive');

            $foreignData = \Crush\Collection::transform((array) $model->find([$primaryKey => $foreignIdList], null, null, null, null, $relatedRecursive), $primaryKey);

            foreach ($results as $x => $row) {
                $row[$foreignDataKey] = [];
                !empty($row[$foreignConfig['key']]) && array_key_exists($row[$foreignConfig['key']], $foreignData) && ($row[$foreignDataKey] = $foreignData[$row[$foreignConfig['key']]]);
                $results[$x] = $row;
            }
        }
        return $results;
    }

    /**
     * Collect related data
     * @param data row to get related data
     * @param relatedNeedList filter the relations that will be collected
     * @access public
     */
    public function getHasMany($data, $relatedNeededList = []) {
        $relations = \Crush\Collection::filter($this->getAttr('foreignKeys'), function($relationData, $relationKey) use($relatedNeededList) {
                    return $relationData['type'] === \HBasis\HASMANY && (empty($relatedNeededList) || in_array($relationKey, $relatedNeededList, true));
                });

        foreach ($relations as $foreignDataKey => $foreignConfig) {
            $primaryKey = empty($foreignConfig['primaryKey']) ? $this->getAttr('primaryKey') : $foreignConfig['primaryKey'];
            !is_array($primaryKey) && ($primaryKey = [$primaryKey]);
            
            $data[$foreignDataKey] = [];
            
            // checks if the relation unique key is available
            $pkCheck = array_filter($primaryKey, function ($key) use($data) {
                return array_key_exists($key, $data) ? $data[$key] : null;
            });
            if(count($primaryKey) > count($pkCheck)) {
                continue;
            }
            
            $id = implode($this->getAttr('foreignKeysSeparator'), array_map(function ($key) use($data) {
                return array_key_exists($key, $data) ? $data[$key] : null;
            }, $primaryKey));

            $model = $this->loadModelInstance($foreignConfig['model']);
            $relatedRecursive = array_key_exists('recursive', $foreignConfig) ? $foreignConfig['recursive'] : $this->getAttr('relatedRecursive');

            if ($foreignData = $model->find([$foreignConfig['key'] => $id], null, null, null, (string) @$foreignConfig['order'], $relatedRecursive)) {
                $data[$foreignDataKey] = $foreignData;
            }
        }
        return $data;
    }

    /**
     * Collect related data for a list of results
     * @param results collection to get related data for
     * @param relatedNeedList filter the relations that will be collected
     * @access public
     */
    public function findHasMany($results, $relatedNeededList = []) {
        if (empty($results)) {
            return $results;
        }

        $relations = \Crush\Collection::filter($this->getAttr('foreignKeys'), function($relationData, $relationKey) use($relatedNeededList) {
                    return $relationData['type'] === \HBasis\HASMANY && (empty($relatedNeededList) || in_array($relationKey, $relatedNeededList, true));
                });

        foreach ($relations as $foreignDataKey => $foreignConfig) {
            $foreignKeysSeparator = $this->getAttr('foreignKeysSeparator');
            $primaryKey = empty($foreignConfig['primaryKey']) ? $this->getAttr('primaryKey') : $foreignConfig['primaryKey'];
            !is_array($primaryKey) && ($primaryKey = [$primaryKey]);
            
            $collectionKey = implode('+'.$foreignKeysSeparator.'+', $primaryKey);
//            printf('<pre>%s</pre>', var_export($collectionKey, true));
            $idList = \Crush\Collection::transform($results, '', [$collectionKey], ['flatten', 'removeEmpty', 'callbacks'=>[$collectionKey=>function($value) use($foreignKeysSeparator) {
                return empty($foreignKeysSeparator) ? $value : preg_replace('/^[\\'.$foreignKeysSeparator.']/', '', $value);
            }]]);

            $model = $this->loadModelInstance($foreignConfig['model']);
            $relatedRecursive = array_key_exists('recursive', $foreignConfig) ? $foreignConfig['recursive'] : $this->getAttr('relatedRecursive');

            $foreignData = empty($idList) ? [] : \Crush\Collection::transform((array) $model->find([$foreignConfig['key'] => $idList], null, null, null, (string) @$foreignConfig['order'], $relatedRecursive), $foreignConfig['key'], [], ['array']);
//            printf('<pre>%s</pre>', var_export($foreignData, true));die('s');
            if(!empty($foreignData))
            foreach ($results as $x => $row) {
                $key = $this->compositeKeyCompose($primaryKey, $row);
                
                $row[$foreignDataKey] = [];
                in_array($key, array_keys($foreignData)) && ($row[$foreignDataKey] = $foreignData[$key]);
                $results[$x] = $row;
            }
        }
        return $results;
    }
    
    public function compositeKeyCompose($key, $data) {
        $foreignKeysSeparator = $this->getAttr('foreignKeysSeparator');
        $result = implode($this->getAttr('foreignKeysSeparator'), array_map(function ($key) use($data) {
            return array_key_exists($key, $data) ? $data[$key] : null;
        }, $key));
        
        if(!empty($foreignKeysSeparator)) {
            $result = preg_replace('/^[\\'.$foreignKeysSeparator.']/', '', $result);
        }
        
        return $result;
    }

    /**
     * Collect related data
     * @param data row to get related data
     * @access public
     */
    public function getHasAndBelongsToMany($data, $relatedNeededList = []) {
        $relations = \Crush\Collection::filter($this->getAttr('foreignKeys'), function($relationData, $relationKey) use($relatedNeededList) {
                    return $relationData['type'] === \HBasis\HASANDBELONGSTOMANY && (empty($relatedNeededList) || in_array($relationKey, $relatedNeededList, true));
                });

        foreach ($relations as $foreignDataKey => $foreignConfig) {
            $data[$foreignDataKey] = [];
            if (!($id = array_key_exists($this->getAttr('primaryKey'), $data) ? $data[$this->getAttr('primaryKey')] : null)) {
                continue;
            }

            $model = $this->loadModelInstance($foreignConfig['model']);
            $bridge = $this->loadModelInstance($foreignConfig['bridge']);
            $relatedRecursive = array_key_exists('recursive', $foreignConfig) ? $foreignConfig['recursive'] : $this->getAttr('relatedRecursive');

            if ($foreignData = $bridge->find([$foreignConfig['keys'][0] => $id], null, null, null, (string) @$foreignConfig['order'], \HBasis\NORELATED)) {
                foreach ($foreignData as $x => $foreignDataItem) {
                    $foreignData[$x][$foreignConfig['modelDataKey']] = $model->get($foreignDataItem[$foreignConfig['keys'][1]], null, $relatedRecursive);
                }
                $data[$foreignDataKey] = $foreignData;
            }
        }
        return $data;
    }

    /**
     * Load model
     * @param string $model
     * @param boolean $setDbConfig defines if db config list will me sended to the new model
     * @return object instance
     */
    public function loadModelInstance($model, $setDbConfig=true) {
        $modelShortName = \Crush\Basic::gcsn($model);
        if (!(@$this->model[$modelShortName]) || !is_object(@$this->model[$modelShortName])) {
            $setDbConfig && static::setDbConfig($model);
            $this->model[$modelShortName] = is_string($model) ? (new $model()) : $model;
        }

        return $this->model[$modelShortName];
    }
    
    /**
     * Alias for loadModelInstance
     */
    public function loadModel($model, $setDbConfig=true) {
        return $this->loadModelInstance($model, $setDbConfig);
    }
    
    /**
     * Load a model from a foreignkey mapped
     */
    public function loadFkModel($foreignName, $setDbConfig=true) {
        return $this->loadModelInstance($this->getAttr('foreignKeys')[$foreignName]['model'], $setDbConfig);
    }
    
    /**
     * Apply a list of events
     * @param array $events
     */
    public function _applyEvents($events=[]) {
        empty($events) && ($events = $this->getAttr('events'));
        foreach($events as $event) {
            $this->_applyEvent($event[0],$event[1]);
        }
    }
    
    /**
     * Apply a event
     * @param string $name
     * @param function $fn
     */
    public function _applyEvent($name, $fn) {
        !is_array($name) && ($name = [$name]);
        array_unshift($name, \Crush\Basic::gcsn($this));
        $name = implode('.', $name);
        
        $this->getEventManager()->on($name,$fn);
    }
    
    /**
     * Generic Event Dispatcher
     * @param string $name
     * @param array $data
     * @return void
     */
    public function dispatchEvent($name, $data=[]) {
        !is_array($name) && ($name = [$name]);
        array_unshift($name, \Crush\Basic::gcsn($this));
        $name = implode('.', $name);
        
        $this->getEventManager()->dispatch(new \Cake\Event\Event($name, $this, $data));
    }
    
    /**
     * Set Db Config
     */
    public static function setDbConfig($class) {
        foreach (static::$connConfigList as $dbconfigname => $dbconfig) {
            $class::setConnConfig($dbconfigname, $dbconfig);
        }
    }

    /** set access to global getters and setters as public */
    public function getAttr($name, $forceDefault = false) {
        return $this->_getAttr($name, $forceDefault);
    }

    public function setAttr($name, $value) {
        return $this->_setAttr($name, $value);
    }

    public function explicitAttr($name) {
        return $this->_explicitAttr($name);
    }

    /**
     * Implements modelAttrDefaults to all models
     * @return array
     */
    public function getAttrProperty() {
        return array_merge($this->attrDefaults, (array) @$this->modelAttrDefaults);
    }

    /**
     * Runs update on data under conditions
     * @param conditions for update
     * @param data to be updated
     * @access public
     */
    abstract public function _updateAll($conditions, $data);

    /**
     * Delete rows
     * @param conditions group of filters
     * @access public
     */
    abstract public function _deleteAll($conditions = []);

    /**
     * Add conditions to query recursively diff arrays from common values
     * @param $query
     * @param $conditions
     */
    abstract public function where($query, $conditions);

    /**
     * Order results
     * @param $query
     * @param $conditions
     */
    abstract public function orderby($query, $orderby);

    /**
     * Returns query builder instance
     */
    abstract public function getQueryBuilder();

    /**
     * Trick to paginate easily with mysql
     * @param $query
     * @param $limit number of rows
     * @param $page use page numbers 0, 1, 2, 3, ...
     */
    abstract public function limit($query, $limit, $page);

    /**
     * Fetch results
     * @param object $query
     * @access public
     */
    abstract public function fetchAll($query);

    /**
     * Affected rows
     * @param object $query
     * @access public
     */
    abstract public function affectedRows($query);

    /**
     * Last Inserted Id
     * @param object $query
     * @access public
     */
    abstract public function lastInsertId($query);
    
    /**
     * Quote identifier
     * @param string $field
     * @return string
     */
    abstract public function quoteField($field);
    
    /**
     * Quote string value
     * @param string $value
     * @return string
     */
    abstract public function quote($value);
}
