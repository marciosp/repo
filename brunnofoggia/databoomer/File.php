<?php

namespace DataBoomer;

/**
 * Utility to keep the data of a object into a file
 *
 * @category	Utilities
 * @author	Bruno Foggia
 * @link	https://bitbucket.org/brunnofoggia/databoomer
 */
trait File {

    use Main;

    /**
     * Generate UID
     */
    public function generateUID() {
        return get_called_class();
    }
    
    /**
     * Defines tmp dir to store the data
     */
    public static function getDir() {
        return __DIR__ . '/../../tmp/';
    }

    /**
     * Defines where data will be stored
     */
    public static function getDataStorage() {
        $dir = static::getDir();

        if (!is_dir($dir) || !is_writable($dir)) {
            throw new \Exception("Directory not exist or is not writable. '" . $dir . "'");
        }

        return $dir;
    }

    /**
     * Returns filepath
     * @param string $uid
     * @return string
     */
    public static function getFilePath($uid) {
        return static::getDataStorage() . $uid;
    }

    /**
     * Check if there is a data file for uid
     * @param string $uid
     * @return bool
     */
    public static function checkDataFile($uid) {
        $filepath = static::getFilePath($uid);
        return is_file($filepath);
    }

    /**
     * Restore data by UID from a file
     * @param string $uid
     */
    public static function restoreData($uid) {
        $filepath = static::getFilePath($uid);
        return is_file($filepath) ? unserialize(file_get_contents($filepath)) : null;
    }

    /**
     * Store data into a file named by uid
     * @param string $uid
     */
    public static function storeData($uid, $data) {
        $filepath = static::getFilePath($uid);
        if (touch($filepath)) {
            file_put_contents($filepath, $data);
        } else {
            throw new \Exception('Permission denied to write file ' . $filepath);
        }
    }

    /**
     * Remove data file based on uid
     * @param string $uid
     */
    public static function clearStoredData($uid) {
        $filepath = static::getFilePath($uid);
        is_file($filepath) && unlink($filepath);
    }

}
