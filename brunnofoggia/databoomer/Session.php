<?php

namespace DataBoomer;

trait Session {

    use Main;

    /**
     * Generate UID
     */
    public function generateUID() {
        return session_id();
    }

    /**
     * Build a key for session item
     * @param type $uid
     * @return type
     */
    public static function dataName($uid = '') {
        return __CLASS__ . (!empty($uid) ? '_' . $uid : '');
    }

    /**
     * Restore data by UID from SESSION
     * @param string $uid
     */
    public static function restoreData($uid) {
        return array_key_exists(static::dataName($uid), $_SESSION) ? unserialize($_SESSION[static::dataName($uid)]) : null;
    }

    /**
     * Store data into SESSION named by uid
     * @param string $uid
     */
    public static function storeData($uid, $data) {
        $_SESSION[static::dataName($uid)] = $data;
    }

    /**
     * Clear Session based on uid
     * @param string $uid
     */
    public static function clearStoredData($uid) {
        if (array_key_exists(static::dataName($uid), $_SESSION)) {
            unset($_SESSION[static::dataName($uid)]);
        }
    }

}
