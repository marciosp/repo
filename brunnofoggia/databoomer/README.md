# DataBoomer

[![Minimum PHP Version](http://img.shields.io/badge/php-%3E%3D%205.4-8892BF.svg)](https://php.net/)
[![License](https://img.shields.io/badge/license-MIT-4C9182.svg)](https://opensource.org/licenses/MIT)

Utility to keep the data of a object through requests
 
By implementing this into a class, all the content stored into its "data" property will be held through the next requests

## usage

1. Add in your class one of the following codes

        // for session storage
        use \DataBoomer\Session;
        // for file storage
        use \DataBoomer\File; 

2. Dont forget to implement serialize and unserialize. An basic example would be

        public function __construct() {
            $this->unserialize();
        }

        public function __destruct() {
            $this->serialize();
        }

3. Adding content to data

        $this->addData('name', 'value'); // name can be split with using '.'

4. Reading content to data

        $this->getData('name');

## advanced usage

- How to change session uid where data from \DataBoomer\Session will be stored

        public function generateUID() {
            $uid = my_uid_generator();
            return $uid;
        }

- How to customize \DataBoomer\File directory

        public static function getDir() {
            return __DIR__ . '/my_tmp_dir/';
        }
