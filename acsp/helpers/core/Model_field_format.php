<?php

namespace acsp\helpers\core;

trait Model_field_format {
    
    public function format_created($field, $value, $format, $data) {
        if(empty($data[$this->getAttr('primaryKey')])) {
            return date('Y-m-d H:i:s');
        }
        return false;
    }
    
    public function format_created_by($field, $value, $format, $data) {
        if(empty($data[$this->getAttr('primaryKey')])) {
            return @\acsp\helpers\Auth::getUserData()['id'];
        }
        return false;
    }
    
    public function format_created_by_ip($field, $value, $format, $data) {
        if(empty($data[$this->getAttr('primaryKey')])) {
            return $_SERVER['REMOTE_ADDR'];
        }
        return false;
    }
    
    public function format_modified($field, $value, $format, $data) {
        if(!empty($data[$this->getAttr('primaryKey')])) {
            return date('Y-m-d H:i:s');
        }
        return false;
    }
    
    public function format_modified_by($field, $value, $format, $data) {
        if(!empty($data[$this->getAttr('primaryKey')])) {
            return @\acsp\helpers\Auth::getUserData()['id'];
        }
        return false;
    }
    
    public function format_modified_by_ip($field, $value, $format, $data) {
        if(!empty($data[$this->getAttr('primaryKey')])) {
            return $_SERVER['REMOTE_ADDR'];
        }
        return false;
    }

    public function format_slug($field, $value, $format, $data) {
        $ci = \get_instance();
        if(array_key_exists('name', $data) || array_key_exists('title', $data)) {
            $slugify = new \Cocur\Slugify\Slugify();
            return $slugify->slugify(!empty($data['name']) ? @$data['name'] : @$data['title']);
        }
        return false;
    }

    public function format_old_slug($field, $value, $format, $data) {
        $newSlug = $this->format_slug(null, null, null, $data);
        if(!empty($data['id']) && $newSlug !== false) {
            $item = $this->get($data['id']);
            
            if(!empty($data['slug']) && $item['slug']!==$newSlug) {
                return $item['slug'];
            }
        }
        return false;
    }

}
