<?php

namespace acsp\helpers\core;

defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends \CI_Controller {

    use CBasis,
        \codeigniter\CodeBlaze\Controller {
        CBasis::renderLayout insteadof \codeigniter\CodeBlaze\Controller;
        CBasis::getAttrProperty insteadof \codeigniter\CodeBlaze\Controller;
        CBasis::sendJsonMsg insteadof \codeigniter\CodeBlaze\Controller;

        \codeigniter\CodeBlaze\Controller::renderLayout as CodeBlaze_renderLayout;
    }

    public function __construct($loadModel = true) {
        $this->CB__construct($loadModel);
    }

    /* legacy */
    public function delete($id = NULL) {
        return $this->remove($id);
    }

}
