<?php

namespace acsp\helpers;

date_default_timezone_set('America/Sao_Paulo');

/**
 * Log
 * @author Bruno Foggia
 */
class Log {

    protected static $instance;

    const dir = '/files/';

    protected $id;
    protected $filePath;

    public static function get($customId='', $inputRequestData = true, $holdInstance = true) {
        if (!static::$instance || !empty($customId)) {
            $instance = new static($customId, $inputRequestData);
            if($holdInstance) {
                static::$instance = $instance;
            }
        } else {
            $instance = self::$instance;
        }
        return $instance;
    }

    public function __construct($customId = '', $inputRequestData = true) {
        $this->id = $this->generateId($customId);
        $this->createFile();
        if ($inputRequestData) {
            $this->inputRequestData();
        }
    }

    public function generateId($customId = '') {
        $data = array();
        $data['id'] = uniqid();
        $data['ip'] = (string) @$_SERVER['REMOTE_ADDR'];
        $data['date'] = date('Ymd');
        $data['time'] = date('His');
        $data['user'] = (string) @$_SERVER['PHP_AUTH_USER'];
        $data['custom'] = (string) $customId;

        return static::buildName($data);
    }

    public static function buildName(Array $param, $pad = '') {
        $data = array();
        $data[] = isset($param['id']) ? $param['id'] : $pad;
        $data[] = isset($param['ip']) ? str_replace('.', '_', $param['ip']) : $pad;
        $data[] = isset($param['date']) ? $param['date'] : $pad;
        $data[] = isset($param['time']) ? $param['time'] : $pad;
        $data[] = isset($param['user']) ? $param['user'] : $pad;
        $data[] = isset($param['custom']) ? $param['custom'] : $pad;

        return implode('-', $data);
    }

    public function createFile() {
        $dirPath = static::getDir();
        $this->createDir($dirPath);
        $this->filePath = $dirPath . $this->id;

        $this->add("---\n");
    }

    protected static function getDir() {
        return static::getVendorsDir() . 'acsp/log' . static::dir;
    }

    public static function getVendorsDir() {
        $_paths = array(
            '/var/www/vendors/',
            '/var/www/vendors.com.br/',
            '/srv/www/vendors.com.br/'
        );
        foreach ($_paths as $path) {
            if (is_readable($path . 'detect_environment.php')) {
                return $path;
                break;
            }
        }
    }

    protected function createDir($dirPath) {
        if (!file_exists($dirPath)) {
            @mkdir($dirPath);
            @chmod($dirPath, 0777);
        }
    }

    public function inputRequestData() {
        $data = array();
        $data[] = 'File: ' . $_SERVER['SCRIPT_FILENAME'];
        $data[] = 'Url: ' . $_SERVER['REQUEST_URI'];
        $data[] = 'Method: ' . $_SERVER['REQUEST_METHOD'];
        $data[] = 'Date: ' . date('Y-m-d H:i:s');
        $data[] = 'Remote Ip: ' . (string) @$_SERVER['REMOTE_ADDR'];
        $data[] = 'Remote Port: ' . (string) @$_SERVER['REMOTE_PORT'];
        $data[] = 'Authentication User: ' . (string) @$_SERVER['PHP_AUTH_USER'];
        $data[] = "---\n";

        $this->add(implode("\n", $data));
    }

    public function add($text = '', $breakLine = true) {
        if ($breakLine) {
            $text .= "\n";
        }

        file_put_contents($this->filePath, $text, FILE_APPEND);
        return $this;
    }

    public static function listOfFiles(Array $params = array()) {
        $namePattern = '/^' . static::buildName($params, ('\w*')) . '$/';
        $dir = static::getDir();

        $files = array();
        if ($handle = opendir($dir)) {
            while (false !== ($entry = readdir($handle))) {
                if (preg_match($namePattern, $entry)) {
                    $files[] = $entry;
                }
            }
            closedir($handle);
        }

        return $files;
    }

    public static function readFiles(Array $params) {
        $files = static::listOfFiles($params);
        $dir = static::getDir();

        $data = array();
        foreach ($files as $file) {
            $data[] = array($file, file_get_contents($dir . $file));
        }

        return $data;
    }

    public static function readFile($file) {
        $dir = static::getDir();
        return array($file, file_get_contents($dir . $file));
    }

}
