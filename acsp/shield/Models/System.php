<?php

namespace acsp\shield\Models;

class System extends \HiMax\model\System {
    protected $database = 'painel';
    protected $table = 'PNSISTEMA';

    protected $fieldsAliases = [
        'rota' => 'route',
        'nome' => 'name',
    ];
    
    /**
     * Get A list of systems the user is allowed to access
     */
    public function getSystemList() {
        $systemList = $this->find();
        
        $results = [];
        $legadoAcl = $this->getLegadoAccess(\HiMax\Core::getMe()->getData('user')['id']);
        foreach($systemList as $x => $systemItem) {
            $systemItem['url'] = $this->formatSystemUrl($systemItem['url'], true);
            if(!empty($systemItem['legado']) && !empty($legadoAcl[$systemItem['legado']])) {
                $results[$systemItem['name']] = $systemItem;
            } else if(!empty($systemItem['route'])) {
                @list($module, $controller, $action) = explode('/', $systemItem['route']);
                if(\HiMax\Core::getMe()->checkAccess($controller, $action, $module, null, $systemItem['id'])===true) {
                    $results[$systemItem['name']] = $systemItem;
                }
            }
        }
        ksort($results);
        return $results;
    }
}
